<?php

include 'utils/database.php';
$db = new mainDB();
?>


<!DOCTYPE HTML>
<html lang="en" class="no-js">
<head>
<title>Portfolio | ETHEREAL VISTA</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="Team Ethereal Vista">
		<meta name="description" content="Ethereal Vista based in Delhi, provides website design, website development, graphic design and digital marketing services.">
		<meta name="keywords" content="website design company,website development company,graphic design company, digital marketing company">
<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
<!--End meta info-->
<!--Start stylesheet include-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">
<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css">
<link rel="stylesheet" type="text/css" href="css/isotope.css">

<!--End stylesheet include-->
<!--Start scripts include-->
<script type="text/javascript" src="js/modernizr.js"></script>
<!--End scripts include-->

<link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">

</head>
<body>
<!--Start preloader-->
<div id="preloader">
<img src="images/loader-big.gif" alt="Please Wait">
</div>
<!--End preloader-->
<!--Start header-->
<?php
include 'utils-ui/navbar.php';
?>
<!--End header-->
<!--Start main section-->
<section class="main secondary_page blog_isotope" data-animate-up="header-static" data-animate-down="header-small">
<!--Start title container-->
<div class="title_container t_align_center type_3">
<div class="container">
<ul class="path_list clearfix">
<li><a href="index.php">Home</a></li>
<li><i></i><a href="portfolio.php">Our Portfolio</a></li>
</ul>
<h1>Portfolio</h1>
</div>
</div>
<!--End title container-->
<div class="container">
<div class="portfolio_isotope_container three_columns gallery">
<!--Start portfolio item-->
<?php 
$result = $db->fetch_medium_image();
while($row = $result->fetch_assoc())
{
	$project_name = $db->fetch_project_name($row['project_id']);
	$large_path = $db->fetch_large_image_path($row['project_id']);
	echo '<div class="portfolio_item web-design">
			<div class="colorbox_container">
				<img src="images/'.$row['filepath'].'" alt="'.$project_name.'">
				<a href="images/'.$large_path.'" data-rel="prettyPhoto[portfolio]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
		</div>';
}
?>
<!--End portfolio item-->
</div>
</div>
</section>
<!--End main section-->
<!--Start footer-->
<?php 
include 'utils-ui/footer.php';
?>
<!--End footer-->
<!--Start scripts include-->
<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
		<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80174278-2', 'auto');
  ga('send', 'pageview');

</script>		
</body>
</html>