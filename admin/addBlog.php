<?php
session_start();

include 'utils/db.class.php';
include 'utils/blogPost.class.php';

$blog = new Blog();

$catData = $blog->getCategories();



	if(isset($_POST['addPost']))
	{
		$data = $blog->addPost($_POST);
		if($data)
		header('Location: blog.php');
	}


?>
<!DOCTYPE HTML>
<html>
<head>
<script type="text/javascript" src="js/jquery.js"></script>

		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/script2.js"></script>
		<script type="text/javascript" src="js/bootstrap3-wysihtml5.all.min.js"></script>
		<link rel="stylesheet" href="css/style2.css" />
		<script src="ckeditor/ckeditor.js"></script>
</head>

<?php   
if(isset($_SESSION['logged_in']))
{
	include 'utils/header.php';
?>
<body>
		<div class="container">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h4>Edit Post</h4>
				</div>
				<div class="panel-body">
					<form method="post" action="">
						<div class="form-group">
							<label for="page_title">Page Title</label>
							<input type="text" id="page_title" name="page_title"  class="form-control" />
						</div>
						
						<div class="form-group">
							<label for="meta_keyword">Meta Keywords</label>
							<input type="text" id="meta_keyword" name="meta_keyword"  class="form-control" />
						</div>
						
						<div class="form-group">
							<label for="title">title</label>
							<input type="text" id="title" name="title"  class="form-control" />
						</div>
						
						<div class="form-group">
							<label for="meta_desc">Meta Description</label>
							<textarea id="meta_desc" name="meta_desc" class="form-control"> </textarea>
						</div>
						
						<div class="form-group">
							<label for="author">Author</label>
							<input type="text" id="author" name="author" class="form-control"  />
						</div>
						
						<div class="form-group">
							<label for="body">Body</label>
							<textarea id="body" name="body" class="form-control"> </textarea>
						</div>
						
						<div class="form-group">
							<label for="body">Photo</label>
							<div id="photo_container">
							</div>
							<span class="upload_btn" onclick="show_popup('popup_upload')">Upload Photo</span>
							
							
						</div>
						
						<div class="form-group">
							<label for="cat_id">Categories</label>
							<select id="cat_id" name="cat_id"  class="form-control">
							<?php 
							foreach($catData as $cData) 
							{
							?>
							
								<option value="<?=$cData['id']?>"><?=$cData['name']?></option>
								<?php
							}
							?>
							</select>
						</div>
					
						
						<input type="submit" class="btn btn-success" name="addPost"/>
					</form>
				</div>
			</div>
			
			<!-- The popup for upload new photo -->
			<div id="popup_upload">
				<div class="form_upload">
					<span class="close" onclick="close_popup('popup_upload')">x</span>
					<h2>Upload photo</h2>
					<form action="upload_photo.php" method="post" enctype="multipart/form-data" target="upload_frame" onsubmit="submit_photo()">
						<input type="file" name="photo" id="photo" class="file_input">
						<div id="loading_progress"></div>
						<input type="submit" value="Upload photo" id="upload_btn">
					</form>
					<iframe name="upload_frame" class="upload_frame"></iframe>
				</div>
			</div>

			<!-- The popup for crop the uploaded photo -->
			<div id="popup_crop" style="z-index:999999999">
				<div class="form_crop">
					<span class="close" onclick="close_popup('popup_crop')">x</span>
					<h2>Crop photo</h2>
					<!-- This is the image we're attaching the crop to -->
					<img id="cropbox" />
					
					<!-- This is the form that our event handler fills -->
					<form>
						<input type="hidden" id="x" name="x" />
						<input type="hidden" id="y" name="y" />
						<input type="hidden" id="w" name="w" />
						<input type="hidden" id="h" name="h" />
						<input type="hidden" id="photo_url" name="photo_url" />
						<input type="button" value="Crop Image" id="crop_btn" onclick="crop_photo()" />
					</form>
				</div>
			</div>
		</div>
	
<?php
}
?>

<script>
CKEDITOR.replace( 'body' , {
    filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
    filebrowserImageBrowseUrl: 'ckfinder/ckfinder.html?type=Images',
    filebrowserFlashBrowseUrl: 'ckfinder/ckfinder.html?type=Flash',
    filebrowserUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
    filebrowserImageUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
    filebrowserFlashUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
});
</script>
</body>
</html>