<?php
	ob_flush();
	session_start();
	require_once 'utils/admin_utility.php';
	$admin_image = new AdminImages();
	$success = 0;
	if(isset($_POST['reset']))
	{
		$_SESSION['selected_cat'] = -1;
		$_SESSION['selected_sub_cat'] = -1;
		unset($data);
	}
	if(!isset($_SESSION['selected_sub_cat']) && !isset($_SESSION['selected_cat']))
	{
		$_SESSION['selected_cat'] = -1;
		$_SESSION['selected_sub_cat'] = -1;
	}
	if($_SESSION['selected_cat'] != -1)
	{
		$raw_sub_categories = $admin_image->fetch_project_sub_categories($_SESSION['selected_cat']);
		$sub_categories = json_decode($raw_sub_categories);
	}
	if(isset($_POST['fetch_category']))
	{
		$raw_sub_categories = $admin_image->fetch_project_sub_categories($_POST['fetch_category']);
		$sub_categories = json_decode($raw_sub_categories);
		$_SESSION['selected_cat'] = $_POST['fetch_category'];
	}
	if(!isset($_POST['store_content']) && isset($_POST['sub_category']))
	{
		$data = json_decode($admin_image->fetch_content_for_sub_cat($_POST['sub_category']));
		$_SESSION['selected_sub_cat'] = $_POST['sub_category'];
		$success = 2;
	}
	if(isset($_POST['store_content']) && isset($_POST['sub_category']))
	{
		$sub_cat_id = $_POST['sub_category'];
		$data = $_POST['content'];
		$flag = -1;				//New Content
		if(isset($_POST['content_id']))
		{
			$flag = $_POST['content_id'];		//Update
		}
		$success = $admin_image->add_content_for_sub_cat($sub_cat_id, $data, $flag);
		$_SESSION['selected_cat'] = -1;
		$_SESSION['selected_sub_cat'] = -1;
	}

	//fetching and storing categories globally to be used in lists
	$raw_categories = $admin_image->fetch_project_categories();
	$categories = json_decode($raw_categories);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Content For Pages</title>
</head>
<body>
<?php
	require_once 'utils/header.php';
?>
<div class="container">
	<p class="alert alert-info">Note:</p>
	<blockquote>
		<h4><sup>*</sup>Textbox supports HTML tags.</h4>
		<h4><sup>*</sup>Hit Reset Button 2 times if the data is still displayed in Text Box.</h4>
	</blockquote>
	<?php
	if($success == 1)
	{
		echo '<h4 id="message">Operation completed successfully.</h4>';
	}
	else if($success == -1)
	{
		echo '<h4 id="message"> Operation failed. Please try later.</h4>';
	}
	?>
</div>
<hr>
<div class="container">
	<form class="col-xs-4" action="" method="post">
		<select onchange="this.form.submit()" name="fetch_category" class="form-control">
		<option disabled selected>Select a Category</option>
		<?php
			foreach ($categories as $category) 
			{
				echo '<option value="'.$category->id.'"';
				if(strcmp($_SESSION['selected_cat'], $category->id) == 0)
					echo 'selected>';
				else
					echo '>';
				echo $category->category_name.'</option>';
			}
		?>
		</select>
	</form>
	<form name="sub_cat" action="" method="post">
	<div class="col-xs-4">
		<select onchange="this.form.submit()" name="sub_category" class="form-control">
		<?php
			echo '<option disabled selected>Select a Sub category.</option>';
			foreach ($sub_categories as $c) 
			{
				echo '<option value="'.$c->id.'"';
				if(strcmp($_SESSION['selected_sub_cat'], $c->id) == 0)
					echo 'selected>';
				else
					echo '>';
				echo $c->sub_category_name.'</option>';
			}
		?>
		</select>
	</div>
	<button class="btn btn-danger pull-right" type="submit" name="reset">Reset</button>
	<br>
	<hr>
		 <div class="box-header">
			<h3 class="box-title">Content Editor</h3>
			<div class="box-body pad">
			<?php
				if($success == 2 && $data == NULL)
				{
					echo '<textarea style="height: 300px;" class="form-control textarea" id="content" name="content" data-provide="markdown" rows="10"></textarea>';
				}
				if($success == 2 && $data != NULL)
				{
					echo '<textarea style="height: 300px;" class="form-control textarea" id="content" name="content" data-provide="markdown" rows="10">'.$data->content.'</textarea>';
					echo '<input type="hidden" name="content_id" value="'.$data->id.'">';
				}
				else if($success != 2)
				{
					echo '<textarea style="height: 300px;" disabled class="form-control textarea" id="content" name="content" data-provide="markdown" rows="10"></textarea>';
				}
			?>
			</div>
		</div>
		<br>
		<button name="store_content" class="btn btn-info pull-right" type="submit"> Store Content </button>
	</form>
</div>

<script>
        $(function () {
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });
</script>
</body>
</html>