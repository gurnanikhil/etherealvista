<?php
	session_start();
	include 'utils/admin_utility.php';
	$errcheck = 0;
	$admin_operations = new MiscOperation();
	if(isset($_POST['replyQuery']))
	{
		$reply = htmlspecialchars($_POST['reply']);
		$id = $_POST['id'];
		$email = $_POST['email'];	
		$name = $_POST['name'];
		$state = $_POST['query_state'];
		$sent = $admin_operations->replyQuery($id, $name, $email, $reply, $state);
		if($sent == 1)
			$errcheck = 1;
		else if($sent == -1)
			$errcheck = -1;
	}
?>

<!DOCTYPE html>
<html>
<?php
	include 'utils/header.php';
?>
<body>
<div class="container">
<blockquote>
	<h4><sup>*</sup>Please wait for a few seconds after each operation to refresh the data.</h4>
	<h4><sup>*</sup>Click on a Query to expand it. </h4>
</blockquote>
<div class = "panel panel-success theme-color">
<div class="panel-heading">
	<h4>Queries</h4>
</div>
<div class="panel-body">
<?php
if($_SESSION['logged_in'] == 1)
{
	if($errcheck == 1)
	{
		echo '<h3 id="alert alert-info message">Your query has been replied/updated. Please wait..</h3></div>';
	}
	else if($errcheck == -1)
	{
		echo '<h3 id="alert alert-warning message">Some error occurred.</h3></div>';
	}
	$raw_queries = $admin_operations->fetch_query();
	$queries = json_decode($raw_queries);
	foreach($queries as $query)
	{
		echo '<div class="panel panel-success" id="parent">';	//opening panel tag
		echo '<div class="panel-heading"><a href="#details'.$query->id.'" data-toggle="collapse">';
		echo '<h4>'.$query->query.'</h4>';
		echo '</a></div>';
		echo '<div data-parent="#parent" class="panel-body collapse" id="details'.$query->id.'">
			<form method="post" action="">
			<input type="hidden" name="id" value="'.$query->id.'">
			<input type="hidden" name="email" value="'.$query->email.'">
			<input type="hidden" name="name" value="'.$query->name.'">
			<div class="input-group-xs">
				<label>Email ID (Click to email Directly)</label>
				<a href="mailto:'.$query->email.'">
				<input readonly="true" style="width:400px;" readonly type="email" class="form-control" value="'.$query->email.'">
				</a>
				<br>
				<label>Query/Question</label>
				<textarea readonly="true" class="form-control">'.$query->query.'</textarea>';?>
				<br>
				<label>Query State</label><select name="query_state" class="form-control" style="width:400px; display:block;">
					<option <?php if($query->state_id == 1) echo 'selected' ?> value="1">Pending</option>
					<option <?php if($query->state_id  == 2) echo 'selected' ?> value="2">In Progress</option>
					<option <?php if($query->state_id  == 3) echo 'selected' ?> value="3">Solved</option>
				</select>
		<?php		
			echo '	<br>
				<label>Answer</label><textarea id="reply" name="reply" class="form-control">'.$query->last_reply.'</textarea>
				<br>
				<button type="submit" name="replyQuery" class="btn btn-info pull-right">Reply
				</button>
			</div></form>';
		echo '</div>';
		echo '</div>';	//closing panel tag
	}
}
else
{
	header('Location: index.php');
}
?>
</div>
</div>
</body>
</html>