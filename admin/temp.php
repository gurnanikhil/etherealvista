<?php
include '../utils/database.php';
if(isset($_POST['addImage']))
{
    if(isset($_FILES))
    {
        $description_1 = $_POST['description_1'];
        $description_2 = $_POST['description_2'];
        $target = "../images/";
        $targetfile_1 = $target.basename($_FILES['file_1']['name']);
        $targetfile_2 = $target.basename($_FILES['file_2']['name']);

        if(move_uploaded_file($_FILES['file_1']['tmp_name'], $targetfile_1))
            echo '<h4 class="container">Successful in uploading 1st Image.</h4>';
        if(move_uploaded_file($_FILES['file_2']['tmp_name'], $targetfile_2))
            echo '<h4 class="container">Successful in uploading 2nd Image.</h4>';


        $dbobj = new mainDB();
        $val = $dbobj->addNewImage($_FILES['file_1']['name'], $description_1);
        $val_2 = $dbobj->addNewImage($_FILES['file_2']['name'], $description_2);
        if($val == 1 && $val_2 == 1)
            echo '<h4 class="container">New Images have been added in the DB and slideshow.</h4>';
    }
}
?>