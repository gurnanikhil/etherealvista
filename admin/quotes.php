<?php
session_start();
include '../utils/database.php';
include 'adminUtility.php';
?>
<!DOCTYPE HTML>
<html>
<body>
<?php   
if(isset($_SESSION['logged_in']))
{
	include 'utils/header.php';
	$dbobj = new mainDB();
	$result = $dbobj->fetchQuotes();
?>
	<div class="container" id="showQuotes">
		<div class="container">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h4>Quotes in the DB</h4>
				</div>
				<div class="panel-body">
					<table class="table table-hover" border="">
							<tr>
								<thead class="thead thead-light">
									<th>ID</th>
									<th>Quote</th>
									<th>Author</th>
									<th>Selected</th>
									<th></th>
								</thead>
							</tr>
						<?php
							while($row = $result->fetch_assoc())
							{
								echo '<tr>';
								echo '<td>'.$row['id'].'</td>
									  <td>'.$row['quote'].'</td>
									  <td>'.$row['author'].'</td>
									  <td><form action="" method="post"><input name="include" type="checkbox" ';
								if($row['selected'] == 1)
									echo 'checked></td>';
								else
									echo '></td>';
								echo '<td><button type="submit" name="update" value="'.$row['id'].'" class="btn btn-info">Update</button>
									  <button type="submit" name="delete" value="'.$row['id'].'" class="btn btn-danger">Delete</button></form></td>
								</tr>';
							}
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
	
<?php
}
?>
</body>
</html>

<?php
if(isset($_POST['update']))
{
	$id = $_POST['update'];
	if(isset($_POST['include']))
	{
		$dbobj = new mainDB();
		$check = $dbobj->updateQuote($id, 1);
		if($check)
		{
			echo '<meta http-equiv="refresh" content="0">';
		}
	}
	else
	{
		$dbobj = new mainDB();
		$check = $dbobj->updateQuote($id, 0);
		if($check)
		{
			echo '<meta http-equiv="refresh" content="0">';
		}
	}
}
?>

<?php
if(isset($_POST['delete']))
{
	$id = $_POST['delete'];
	$dbobj = new mainDB();
	$check = $dbobj->deleteQuote($id);
	if($check)
	{
		echo '<meta http-equiv="refresh" content="0">';
	}
	else
	{
		echo 'The quote could not be deleted because of DB Error.';
	}
}
?>