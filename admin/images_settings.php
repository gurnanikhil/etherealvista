<?php
session_start();
if($_SESSION['logged_in'] == 1)
{
	include 'utils/admin_utility.php';
	$admin_image = new AdminImages();
	$success = 0;
	global $projects;
	$projects = json_decode($admin_image->fetch_all_images_portfolio());
	if(isset($_POST['fetch_category']))
	{
		if($_POST['fetch_category'] == 0)
		{

			$projects = json_decode($admin_image->fetch_all_images_portfolio());
		}
		else
		{
			$projects = json_decode($admin_image->fetch_product_images_for_category($_POST['fetch_category']));
		}
	}
	if(isset($_POST['add_project']))				//add new image
	{
		$success = $admin_image->add_new_image($_POST, $_FILES);
	}
	if(!isset($_POST['delete']) && isset($_POST['project_id']))		//checkbox toggle
	{
		$success = $admin_image->include_image($_POST);
            		echo '<meta http-equiv="refresh" content="0">';	
	}
	if(isset($_POST['delete']))		//delete button
	{
		$success = $admin_image->delete_image($_POST);
	}
	if(isset($_POST['delete_category']))
	{
		$success = $admin_image->delete_category($_POST['category_id']);
	}
	if(isset($_POST['add_category']))
	{
		$success = $admin_image->add_new_category($_POST['category_name']);
	}
	if(isset($_POST['add_sub']))
	{
		$success = $admin_image->add_new_sub_category($_POST['category'], $_POST['sub_name']);
	}
	if(isset($_POST['delete_sub']) && isset($_POST['sub_category_id']))
	{
		$success = $admin_image->delete_sub_category($_POST['sub_category_id']);
	}
	//fetching and storing categories globally to be used in lists
	$raw_categories = $admin_image->fetch_project_categories();
	$categories = json_decode($raw_categories);

	//fetching and storing sub categories globally 
	$raw_sub_cats = $admin_image->fetch_project_categories();
	$sub_cats = json_decode($raw_sub_cats);
?>

<!DOCTYPE html>
<html>
	<?php
		include 'utils/header.php';
	?>
<body>
<div class="container-fluid">
	<ul class="list-inline pull-right" type="none">
		<li><a href="#catagory" data-toggle="modal">
			<button type="" class="btn btn-info">
				View/Add Categories
			</button>
		</a></li>
		<li><a href="#add_sub_category" data-toggle="modal">
			<button type="" class="btn btn-info">
				Add A New Subcategory
			</button>
		</a></li>
		<li><a href="#add_image" data-toggle="modal">
			<button type="" class="btn btn-info">
				Add A New Project
			</button>
		</a></li>
	</ul>
	<blockquote>
		<h4><sup>*</sup>Please wait for a few seconds after each operation to refresh the data.</h4>
	</blockquote>
	<div class="container-fluid">
	<form action="" method="post" class="col-xs-4">
		<label>Select a category </label>
		<select onchange="this.form.submit()" class="form-control" name="fetch_category">
			<option selected disabled>Select a category</option>
			<option value="0">All Categories</option>
			<?php
				foreach ($categories as $category) 
				{
					echo '<option value="'.$category->id.'">'.$category->category_name.'</option>';
				}
			?>
		</select>
	</form>
	</div><br>
<?php
	if($success == 1)
	{
		echo '<h4 id="alert alert-info message">Operation completed successfully.</h4>';
	}
	else if($success == -1)
	{
		echo '<h4 id="alert alert-warning message"> Operation failed. Please try later.</h4>';
	}
	echo '<div class="panel panel-info">
		<div class="panel-heading">
			<h4>Images for Portfolio</h4>
		</div>
		<div class=" container-fluid panel-body">
			<div class="table-responsive">
				<table class="table table-hover table-bordered">
				<thead class="thead-inverse">
					<tr>
						<th>Project ID</th>
						<th>Project Image</th>
						<th>Project Name</th>
						<th>Project Category</th>
						<th colspan="2">Options</th>
					</tr>
				</thead>';
			foreach ($projects as $project) 
			{
				$temp = $project->id;
				echo '<tr>';
					echo '<td>'.$project->id.'</td>';
					echo '<td><label>Small Image</label>
						<a class="wat" href="#image_modal" data-toggle="modal"><img class="kden" style="height:100px; width: 200px;" src="'.$project->small_path.'"></a><hr>
						<label>Medium Image</label>
						<a class="wat" href="#image_modal" data-toggle="modal"><img class="kden" style="height:100px; width: 200px;" src="'.$project->medium_path.'"></a><hr>
						<label>Large Image</label>
						<a class="wat" href="#image_modal" data-toggle="modal"><img class="kden" style="height:100px; width: 200px;" src="'.$project->large_path.'"></a>
					</td>';
					echo '<td>'.$project->project_name.'</td>';
					echo '<td>'.$project->category_name.'</td>';
					echo '<td><form action="" method="post"><input type="hidden" name="project_id" value="'.$project->id.'">
						<input type="checkbox" name="include" onchange="this.form.submit()"';
						if($project->include == 1)
						{
							echo 'checked> ';
						}
						else
						{
							echo '> ';
						} 
						echo '<label>Include In Slideshow</label>

						<td><input type="submit" name="delete" class="btn btn-danger" value="Delete"></form></td>';
				echo '</tr>';
			}

			echo '	</table>
			</div>
		</div>
	</div>';
?>
<?php
}
else
{
	header('Location: index.php');
}
?>

<!--MODAL FOR ADDING NEW IMAGE -->
<div class="container">
	<div class="modal fade col-xs-5 container-fluid" id="add_image">
		<div class="modal-content">
			<div class="modal-header">
				<h3>Add New Project and Images (3 Sizes)</h3>
			</div>
			<div class="modal-body">
				<form autocomplete = "off" action="" enctype="multipart/form-data" method="post">
					<div class="input-group-lg container-fluid">
						<input placeholder="Project Name" required type="text" class="form-control" name="description">
						<br>
						Short Image: <input required class="form-control" name="file_1" id="file_1" accept="image/*" placeholder="Image Size Small" type="file">
						<br>
						Medium Image: <input required class="form-control" name="file_2" id="file_2" accept="image/*" placeholder="Image Size Medium" type="file">
						<br>
						Large Image: <input required class="form-control" name="file_3" id="file_3" accept="image/*" placeholder="Image Size Large" type="file">
						<br>
						<input type="submit" id="add_project" name="add_project" class="btn btn-primary pull-right" placeholder="Add Files">
					</div>
				</form>
			</div>
			<div class="modal-footer pull-right">
				<span class="btn btn-info" data-dismiss="modal">&times;</span>
			</div>
		</div>
	</div>
</div>
</div>
<div class="container">
<div id="catagory" class="modal fade col-xs-5 container-fluid">
	<div class="modal-content">
		<div class="modal-header">
			<h4>View/Add/Remove Categories</h4>
		</div>
		<div class="modal-body">
		<?php
		if($categories != NULL)
		{
		?>
			<h4>You have following categories:</h4>
			<div class="table-responsive container-fluid">
				<table class="table table-hover table-bordered">
					<thead class="thead-inverse">
						<tr>
							<th>Category ID</th>
							<th>Category Name</th>
							<th></th>
						</tr>
					</thead>
				<?php
					foreach($categories as $c)
					{
						echo '<tr>';
							echo '<td>'.$c->id.'</td>';
							echo '<td>'.$c->category_name.'</td>';
							echo '<td><form autocomplete = "off" method="post"><input type="hidden" name="category_id" value="'.$c->id.'"><button name="delete_category" type="submit" class="btn btn-danger">Delete</button></form></td>';
						echo '</tr>';
					}
				?>
				</table>
			</div>
		<?php
		}
		else
		{
			echo '<h5 class="alert alert-info">No categories found!</h5>';
		}
		?>
			<form autocomplete="off" class="form-inline" method="post">
				<input class="form-control" type="text" name="category_name" placeholder="Category Name">&nbsp;&nbsp;&nbsp;<button type="submit" name="add_category" class="btn btn-primary">Add New Category</button>
			</form>
		</div>
		<span class="modal-footer btn btn-info pull-left" data-dismiss="modal">&times;</span>
	</div>
</div>
</div>
<div class="container">
	<div class="modal fade col-xs-6 container" id="image_modal">
		<div class="modal-body">
			<button class="btn btn-info modal-header pull-right" data-dismiss="modal">&times;</button>
			<img id="image" class="img-responsive">
		</div>
	</div>
</div>
<div class="container">
	<div class="modal col-xs-6 container" id="upload_image">
		<div class="modal-body">
		<?php
			echo $temp;
		?>
		</div>
	</div>
</div>
<div class="container">
	<div class="modal col-xs-5 container" id="add_sub_category">
		<div class="modal-content">
			<span class="btn btn-info pull-right" data-dismiss="modal">&times;</span>
			<div class="modal-header">
				<h4>View/Add New Subcategory</h4>
			</div>
			<div class="modal-body">
			<?php
			if($sub_cats != NULL)
			{
			?>
			<h4>You have following Sub categories:</h4>
				<div class="table-responsive container-fluid">
					<table class="table table-hover table-bordered">
						<thead class="thead-inverse">
							<tr>
								<th>Sub Category ID</th>
								<th>SubCategory Name</th>
								<th>Parent Category Name</th>
								<th></th>
							</tr>
						</thead>
					<?php
						foreach($sub_cats as $c)
						{
							echo '<tr>';
								echo '<td>'.$c->id.'</td>';
								echo '<td>'.$c->sub_category_name.'</td>';
								echo '<td>'.$c->category_name.'</td>';
								echo '<td><form autocomplete = "off" method="post"><input type="hidden" name="sub_category_id" value="'.$c->id.'"><button name="delete_sub" type="submit" class="btn btn-danger">Delete</button></form></td>';
							echo '</tr>';
						}
					?>
					</table>
				</div>
				<?php
				}
				else
				{
					echo '<h5 class="alert alert-info">No Sub Categories Found!</h5>';
				}
				?>
				<div class="input-group-lg container-fluid">
					<form class="col-xs-8" method="post" action="">
						<select class="form-control" class="form-control" name="category">
							<option selected disabled>Select a category</option>
							<?php
							foreach ($categories as $category) 
							{
								echo '<option value="'.$category->id.'">'.$category->category_name.'</option>';
							}
							?>
							</select>
						<br>
						<input class="form-control" type="text" placeholder="Subcategory Name" name="sub_name">
						<br>
						<button name="add_sub" type="submit" class="btn btn-info">Add New Subcategory</button>
						</select>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<span class="btn btn-info" data-dismiss="modal">&times;</span>
			</div>
		</div>
	</div>
</div>
</body>
</html>