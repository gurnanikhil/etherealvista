<?php

class database
{
    public $dbc;
    private static $instance;
 
    private function __construct()
    {
        $this -> dbhc= new PDO('mysql:host=localhost;dbname=example', "root", "password1234");
    }
 
    //singleton pattern
    public static function getInstance()
    {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }
}

class Blog
{
    
    
/*	public $errflag = 0;
	public function connect()
	{
		$con = new mysqli('localhost', 'root', '', 'brainevy_ethereal');
		if ($con->connect_errno) 
		{
			echo "Failed to connect to MySQL: " . $con->connect_error;
		}
		return $con;
	}

	public function contactus()
	{
		$uname = $_POST['uname'];
		$umail = $_POST['umail'];
		$text = $_POST['message'];
		$query = "INSERT IGNORE INTO `query`(`name`, `email`, `query`, `query_state_id`) VALUES ('$uname', '$umail', '$text', 1)";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
		$this->contactMail($uname,$umail,$text);
		return $errflag = 0;
	}

	public function fetchImage()
	{
		$con = $this->connect();
		$query = "SELECT * FROM  small_images ORDER BY id DESC;";
		$result = $con->query($query);
		if($result == false)
		{
			echo 'Some Database Error has occurred in getting images.'.$con->mysqli_err_no;
		}
		return $result;
	}
	public function login()
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		$query = "SELECT `password` FROM `admins` where `username`='$username';";
		$con = $this->connect();
		$result = $con->query($query);
		if(!$result)
		{
			echo 'Some Database Error has occurred.';
		}
		$row = $result->fetch_assoc();
		$verify = password_verify($password, $row['password']);
		return $verify;
	}
	public function addNewAdmin($username, $password, $name)
	{
		$password = password_hash($password, PASSWORD_BCRYPT);
		$query = "INSERT IGNORE INTO admins(username, password, name) VALUES('$username', '$password', '$name')";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
		return 'Success';
	}
	public function addNewProject($description)
	{
		$query = "INSERT INTO project(project_name) VALUES('$description')";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
		$query = "SELECT id from project where project_name = '$description';";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
		$row = $result->fetch_assoc();
		return $row['id'];
	}
	public function addNewImage($file, $flag, $id)
	{
		$query = "";
		if($flag == 1)
			$query = "INSERT INTO small_images(filepath, project_id, include) VALUES('$file', $id, 1)";
		else if($flag == 2)
			$query = "INSERT INTO medium_images(filepath, project_id) VALUES('$file', $id)";
		else if($flag == 3)
			$query = "INSERT INTO large_images(filepath, project_id) VALUES('$file', $id)";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
	}
	public function addNewQuote($quote, $author, $selected)
	{
		$query = "INSERT INTO quotes(quote, author, selected) VALUES('$quote','$author', '$selected')";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
		return $errflag = 1;
	}
	public function fetchQuotes()
	{
		$query = "SELECT * FROM quotes;";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
		return $result;
	}
	public function updateImage($id, $value)
	{
		$query = "UPDATE small_images SET include = $value WHERE id = $id;";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			return 0;
		}
		return 1;
	}
	public function deleteImage($id)
	{
		$query = "DELETE FROM small_images where project_id = $id;";
		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			return 0;
		}

		$query = "DELETE FROM medium_images where project_id = $id;";
		$result = $con->query($query);
		if(!$result)
		{
			return 0;
		}

		$query = "DELETE FROM large_images where project_id = $id;";
		$result = $con->query($query);
		if(!$result)
		{
			return 0;
		}

		$query = "DELETE FROM project where id = $id;";
		$result = $con->query($query);
		if(!$result)
		{
			return 0;
		}
		return 1;
	}
	public function updateQuote($id, $value)
	{
		$query = "UPDATE quotes SET selected = $value WHERE id = $id;";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			return 0;
		}
		return 1;
	}
	public function deleteQuote($id)
	{
		$query = "DELETE FROM quotes where id = $id;";
		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			return 0;
		}
		return 1;
	}
	
	public function contactMail($name,$mail,$query)
	{	
		$callingFunction = debug_backtrace()[1]['function'];
		$this->check_calling_function($callingFunction);
	}
	
	public function check_calling_function($name)
	{
		$data = "test";
		switch($name)
		{
			case 'contactus':
				/*//Sending Mail
				$to = $umail;
				$subject = "Thanks for contacting Us";

				$message = "
				<html>
				<head>
				<title>Thanks For Contacting Us</title>
				</head>
				<body>
				<h1>Thanks Mr/Mrs ".$name."</h1>
				For Contacting Us<br><br><br>
				Your email-id  ".$umail."<br> has been Registered<br>
				We will be shortly with you on query <br><br>~~".$query ."~~
				This is a system generated email. Please do not reply to this email.
				</body>
				</html>
				";

				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

				// More headers
				$headers .= 'From: <customercare@brainybox.com>' . "\r\n";
				$headers .= 'Bcc: recordmail@brainybox.com' . "\r\n";
				mail($to,$subject,$message,$headers);
				";*/
			break;
			
			case 'replyQuery':
			/*//Sending Mail
				$to = $umail;
				$subject = "Thanks for contacting Us";

				$message = "
				<html>
				<head>
				<title>Thanks For Contacting Us</title>
				</head>
				<body>
				<h1>Thanks Mr/Mrs ".$name."</h1>
				For Contacting Us<br><br><br>
				Your email-id  ".$umail."<br> has been Registered<br>
				We will be shortly with you on query <br><br>~~".$query ."~~
				This is a system generated email. Please do not reply to this email.
				</body>
				</html>
				";

				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

				// More headers
				$headers .= 'From: <customercare@brainybox.com>' . "\r\n";
				$headers .= 'Bcc: recordmail@brainybox.com' . "\r\n";
				mail($to,$subject,$message,$headers);
				";*/
			break;
			
		}
		return $data;
	}
	public function fetchQuery()
	{
		$query = "SELECT * FROM query;";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
        	return $result;
	}
	public function replyQuery()
	{
		$id = $_POST['id'];
		$answer = $_POST['answer'];
		$query_state = $_POST['query_state'];
		$name = $_POST['name'];
		$email = $_POST['email'];

		
		$query = "UPDATE Query SET last_reply = '$answer', query_state_id='$query_state'";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
		
		echo 'Replied';
		$this->contactMail($name,$email,$answer);
		return $errflag = 0;
	}
	public function fetch_project_name($id)
	{
		$query = "SELECT project_name FROM project WHERE id = $id;";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
		$row = $result->fetch_assoc();
		return $row['project_name'];
	}
	public function fetch_large_image_path($project_id)
	{
		$query = "SELECT filepath FROM large_images WHERE project_id = $project_id;";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
		$row = $result->fetch_assoc();
		return $row['filepath'];
	}
	public function fetch_medium_image()
	{
		$query = "SELECT * FROM medium_images ORDER BY id DESC";

		$con = $this->connect();

		$result = $con->query($query);
		if(!$result)
		{
			$errflag = -1;
			return $errflag;
		}
		return $result;
	}
}
*/
?>