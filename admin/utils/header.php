<?php
$filename = basename($_SERVER['REQUEST_URI'], '.php');
require_once 'quotesModal.php';
?>
<head>
		<title>Admin Panel</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="">
		<meta name="description" content="">
		<meta name="keywords" content="">
	
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap3-wysihtml5.min.css" />
		<script src="js/jquery.js"></script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
		<script type="text/javascript" src="js/bootstrap3-wysihtml5.all.min.js"></script>

		

		<style>
			body
			{
				padding-top: 75px;
				color: black;
			}
			h1, h2, h3, h4, h5, h6
			{
			    color: black;
			}
			input[type=checkbox]
			{
			  -webkit-appearance:checkbox;
			}
			.dropdown:hover .dropdown-menu 
			{
			    display: block;
			}
			a:hover
			{
				text-decoration: none;
			}
		</style>
		<script type="text/javascript"> 
			$(document).ready( function() {
				$('#message').delay(1000).fadeOut();
			});
		</script>

		<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/jquery.Jcrop.min.css" type="text/css" />
<!-- <script type="text/javascript" src="js/jquery-3.0.0.js"></script> -->
<script src="js/jquery.Jcrop.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
		
</head>
<?php
if($_SESSION['logged_in'] == 0)
{
	echo '<div class="container text-center">
		<div class="modal modal-md fade col-xs-5 container-fluid" id="loginModal">
			<div class="modal-content">
				<div class="modal-header">
					<h4>
						Login
					</h4>
				</div>
				<div class="modal-body">';
				if($check)
				{
					echo '<p class="alert alert-warning alert-dismissible">Wrong Username or Password.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>';
				}
				echo '	<form autocomplete="off" action="" method="post">
						<div class="input-group-lg container-fluid">
							<input required="required" type="text" name="username" class="form-control" id="username" placeholder="Username">
							<br>
							<input required="required" type="password" name="password" class="form-control" id="password" placeholder="Password">
							<br>
							<input type="submit" name="login" id="login" class="btn btn-info pull-right" value="Login"  data-target="#content">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>';
}

else if($_SESSION['logged_in'] == 1)
{
	echo '<div class="container" id="content">

		<div class="navbar theme-color navbar-fixed-top">

			<div class="container-fluid">

				<div class="navbar-header">

					<a href = "index" class="navbar-brand">
						Ethereal Vista
					</a>
					<button type="button" class="navbar-toggle navbar-collapse" data-toggle="collapse" data-target="#navHeader">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse navbar-toggleable-xs" id="navHeader">
					<ul class="nav navbar-nav navbar-right nav-tabs">
						<li class="dropdown"><a href="images_settings">Images</a>
                        </li>	
						<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Quotes<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="quotes">View/Select Available Quotes</a></li>
                                <li><a href="#quotesModal" data-toggle="modal">Add New Quotes</a></li>
                            </ul>';
						echo '<li '; if($filename == "content") echo 'class="active";';echo '><a href="content">Content For Pages</a></li>';	
						echo '<li '; if($filename == "contact") echo 'class="active";';echo '><a href="contact">Queries and Replies</a></li>';	
						echo '<li '; if($filename == "images_settings") echo 'class="active";';echo '><a href="images_settings">Product & Images Settings</a></li>';
						echo '<li '; if($filename == "blog") echo 'class="active";';echo '><a href="blog">blog</a></li>';
						echo '<li><a href="logout">Log Out</a></li>
					</ul>
				</div>

			</div>

		</div>

	</div>
	<div class="navbar theme-color navbar-fixed-bottom">
		<h4 class="navbar-nav navbar-brand" style="color:#ffffff;">';
			$dt = new DateTime("now", new DateTimeZone('Asia/Kolkata'));
			echo 'Time: '.$dt->format('H:i');
	echo '	</h4>
		<a href="../"><h4 class="btn btn-primary pull-right" style="margin-right:30px;">Preview Website</h4></a>
	</div>';
}
?>

<?php

?>