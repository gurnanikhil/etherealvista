<?php
class AdminSession
{
	public function connect()
	{
		$con = new Mysqli("localhost", "root", "password1234", "brainevy_ethereal");
		if(!$con)
			die('Some database connection error has occurred.');
		return $con;
	}
	public function login_admin($username, $password)
	{
		$con = $this->connect();

		$query = "SELECT name, password FROM admins where username = '$username';";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;
		}
		$row = $result->fetch_assoc();
		$verify = password_verify($password, $row['password']);
		if($verify)
		{
			$json = array('name'=>$row['name'], 'username' => $username);
			$admin = json_encode($json);
			return $admin;
		}
		else
		{
			return -2;
		}
	}
	public function logout_admin()
	{
		session_destroy();
	}
}

class MiscOperation extends AdminSession
{
	public function fetch_query()
	{
		$con = $this->connect();

		$query = "SELECT * FROM query ORDER BY query_state_id ASC, id DESC;";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;
		}
		$json = array();
		while($row = $result->fetch_assoc())
		{
			$json[] = array('id'=>$row['id'], 'name' => $row['name'], 'email' => $row['email'], 'query'=>$row['query'], 'state_id'=>$row['query_state_id'], 'last_reply'=>$row['last_reply']);
		}
		$queries = json_encode($json);
		return $queries;
	}
	public function replyQuery($id, $name, $email, $reply, $status)
	{
		$con = $this->connect();
		if($reply != NULL)
		{
			$query = "UPDATE query SET last_reply = '$reply', query_state_id = $status WHERE id = $id;";
		}
		else if($reply == NULL)
		{
			$query = "UPDATE query SET query_state_id = $status WHERE id = $id;";
		}

		$result = $con->query($query);
		if(!$result)
		{
			return -1;
		}
		else
		{
			//$this->contactMail($name, $email, 'Re: Ethereal Vista', $reply);
			return 1;
		}

	}

	public function contactMail($name,$mail,$subject,$query)
	{	
		$callingFunction = debug_backtrace()[1]['function'];
		$this->check_calling_function($callingFunction);
	}
	
	public function check_calling_function($name)
	{
		$data = "test";
		switch($name)
		{
			case 'contactus':
				/*//Sending Mail
				$to = $umail;
				$subject = "Thanks for contacting Us";

				$message = "
				<html>
				<head>
				<title>Thanks For Contacting Us</title>
				</head>
				<body>
				<h1>Thanks Mr/Mrs ".$name."</h1>
				For Contacting Us<br><br><br>
				Your email-id  ".$umail."<br> has been Registered<br>
				We will be shortly with you on query <br><br>~~".$query ."~~
				This is a system generated email. Please do not reply to this email.
				</body>
				</html>
				";

				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

				// More headers
				$headers .= 'From: <customercare@brainybox.com>' . "\r\n";
				$headers .= 'Bcc: recordmail@brainybox.com' . "\r\n";
				mail($to,$subject,$message,$headers);
				";*/
			break;
			
			case 'replyQuery':
			/*//Sending Mail
				$to = $umail;
				$subject = "Thanks for contacting Us";

				$message = "
				<html>
				<head>
				<title>Thanks For Contacting Us</title>
				</head>
				<body>
				<h1>Thanks Mr/Mrs ".$name."</h1>
				For Contacting Us<br><br><br>
				Your email-id  ".$umail."<br> has been Registered<br>
				We will be shortly with you on query <br><br>~~".$query ."~~
				This is a system generated email. Please do not reply to this email.
				</body>
				</html>
				";

				// Always set content-type when sending HTML email
				$headers = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

				// More headers
				$headers .= 'From: <customercare@brainybox.com>' . "\r\n";
				$headers .= 'Bcc: recordmail@brainybox.com' . "\r\n";
				mail($to,$subject,$message,$headers);
				";*/
			break;
			
		}
		return $data;
	}
}


class AdminImages extends AdminSession
{
	public function fetch_project_categories()
	{
		$con = $this->connect();

		$query = "SELECT * FROM  categories;";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;	//db error
		}
		else
		{
			$json = array();
			while($row = $result->fetch_assoc())
			{
				$json[] = $row;
			}
			$raw = json_encode($json);
			return $raw;
		}
	}
	public function add_new_image($data, $file)
	{
		$con = $this->connect();
		//Gathering all data first
		$name = $data['description'];

		//Moving the image to the appropriate directory
		$target_short = "../images/short-images/";
		$target_medium = "../images/medium-images/";
		$target_large = "../images/large-images/";

		$targetfile_1 = $target_short.basename($_FILES['file_1']['name']);
		$targetfile_2 = $target_medium.basename($_FILES['file_2']['name']);
		$targetfile_3 = $target_large.basename($_FILES['file_3']['name']);

		move_uploaded_file($_FILES['file_1']['tmp_name'], $targetfile_1);
		move_uploaded_file($_FILES['file_2']['tmp_name'], $targetfile_2);
		move_uploaded_file($_FILES['file_3']['tmp_name'], $targetfile_3);

		//Executing Query for storing Product
		$query = "INSERT INTO project(project_name) VALUES('$name');";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;	//db error
		}
		else 			//if success, now store image in the db
		{
			$project_id = $con->insert_id;
			//Add Small Image
			$query = "INSERT INTO small_images(filepath, project_id, include) VALUES('$targetfile_1', $project_id, 1)";
			$result = $con->query($query);
			if(!$result)
			{
				return -1;	//db error
			}
			//Add Medium Image
			$query = "INSERT INTO medium_images(filepath, project_id) VALUES('$targetfile_2', $project_id)";
			$result = $con->query($query);
			if(!$result)
			{
				return -1;	//db error
			}
			//Add Large Image
			$query = "INSERT INTO large_images(filepath, project_id) VALUES('$targetfile_3', $project_id)";
			$result = $con->query($query);
			if(!$result)
			{
				return -1;	//db error
			}
			else
			{
				return 1;	//ultimate success
			}
		}
	}
	public function fetch_all_images_portfolio()
	{
		$con = $this->connect();

		$query = "SELECT p.id, p.project_name, categories_id,c.category_name, si.filepath as small_path, mi.filepath as medium_path, li.filepath as large_path, si.include FROM `project` p join small_images si on si.project_id = p.id join medium_images mi on mi.project_id = p.id join large_images li on li.project_id = p.id join categories c on p.categories_id = c.id order by p.id DESC, categories_id ASC;";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;	//db error
		}
		else
		{
			$json = array();
			while($row = $result->fetch_assoc())
			{
				$json[] = $row;
			}
			$raw = json_encode($json);
			return $raw;
		}
	}
	public function include_image($data)
	{
		$id = $data['project_id'];
		$con = $this->connect();
		if(isset($data['include']))
		{
			$query = "UPDATE small_images SET include = 1 WHERE project_id = $id;";
			$result = $con->query($query);
			if(!$result)
			{
				return -1;	//db error
			}
			else
			{
				return 1;
			}
		}
		else if(!isset($data['include']))
		{
			$query = "UPDATE small_images SET include = 0 WHERE project_id = $id;";
			$result = $con->query($query);
			if(!$result)
			{
				return -1;	//db error
			}
			else
			{
				return 1;
			}
		}
	}
	public function delete_image($data)
	{
		$id = $data['project_id'];
		$con = $this->connect();
		//Remove Small Image
		// $query = "DELETE FROM small_images WHERE project_id = $id;";
		// $result = $con->query($query);
		// if(!$result)
		// {
		// 	return -1;	//db error
		// }
		// //Remove Medium Image
		// $query = "DELETE FROM medium_images WHERE project_id = $id;";
		// $result = $con->query($query);
		// if(!$result)
		// {
		// 	return -1;	//db error
		// }
		// //Remove Large Image
		// $query = "DELETE FROM large_images WHERE project_id = $id;";
		// $result = $con->query($query);
		// if(!$result)
		// {
		// 	return -1;	//db error
		// }
		$query = "DELETE FROM projects WHERE id = $id;";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;	//db error
		}
		else
		{
			return 1;	//ultimate success
		}
	}
	public function delete_category($cid)
	{
		$con = $this->connect();
		$query = "DELETE FROM categories WHERE id = $cid;";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;	//db error
		}
		else
		{
			return 1;
		}
	}
	public function add_new_category($name)
	{
		$con = $this->connect();
		$query = "INSERT INTO categories(category_name) VALUES('$name');";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;	//db error
		}
		else
		{
			return 1;
		}
	}
	public function fetch_product_images_for_category($cid)
	{
		$con = $this->connect();

		$query = "SELECT p.id, p.project_name, categories_id,c.category_name, si.filepath as small_path, mi.filepath as medium_path, li.filepath as large_path, si.include FROM `project` p join small_images si on si.project_id = p.id join medium_images mi on mi.project_id = p.id join large_images li on li.project_id = p.id join categories c on p.categories_id = c.id where p.categories_id = $cid order by p.id DESC, categories_id ASC;";
		$result = $con->query($query);
		$json = array();

		while($row = $result->fetch_assoc())
		{
			$json[] = $row;
		}

		return json_encode($json);
	}
	public function add_new_sub_category($cat_id, $name)
	{
		$con = $this->connect();
		$query = "INSERT INTO sub_categories(sub_category_name, categories_id) VALUES('$name', $cat_id);";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;	//db error
		}
		else
		{
			return 1;
		}
	}
	public function fetch_project_sub_categories($id)
	{
		$con = $this->connect();
		$query = "SELECT sc.id, sc.sub_category_name, c.category_name FROM `sub_categories` sc Join categories c on c.id = sc.categories_id where c.id = $id;";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;	//db error
		}
		else
		{
			$json = array();
			while($row = $result->fetch_assoc())
			{
				$json[] = $row;
			}
			return json_encode($json);
		}
	}
	public function delete_sub_category($cid)
	{
		$con = $this->connect();
		$query = "DELETE FROM sub_categories WHERE id = $cid;";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;	//db error
		}
		else
		{
			return 1;
		}
	}
	public function add_content_for_sub_cat($sub_id, $data, $flag)
	{
		$con = $this->connect();
		if($flag == -1)
		{
			$query = "INSERT INTO content(content, sub_category_id) VALUES('$data', $sub_id);";
		}
		else if($flag != -1)
		{
			$query = "UPDATE content set content = '$data' where id = $flag;";
		}
		$result = $con->query($query);
		if(!$result)
		{
			return -1;	//db error
		}
		else
		{
			return 1;
		}
	}
	public function fetch_content_for_sub_cat($id)
	{
		$con = $this->connect();
		$query = "SELECT * from content where sub_category_id = $id;";
		$result = $con->query($query);
		if(!$result)
		{
			return -1;	//db error
		}
		else
		{
			$json = $result->fetch_assoc();
			return json_encode($json);
		}
	}
}
?>