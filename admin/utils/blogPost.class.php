<?php

require_once('utils/db.class.php');

class Blog
{
	protected $db ;

    function __construct()
    {
        $this->db = db::getInstance();
    }

	
	public function fetchAllPosts()
    {
        $sql = "SELECT * from blog_post order by timestamp DESC";

        $result = $this->db->query($sql);

        if(!$result)
        {

        }
        return $result;
    }
	
	public function getCategoryById($id)
	{

		$sql = "SELECT name from post_cat WHERE id  = {$id}";
	
		$result = $this->db->query($sql);
		
		if(!$result)
		{
			
		}
		
		return $result;
	}
	
	public function getPostById($id)
	{

		$sql = "SELECT * from blog_post WHERE id  = {$id}";
	
		$result = $this->db->query($sql);
		
		if(!$result)
		{
			
		}
		
		return $result;
	}
	
	public function getCategories()
	{

		$sql = "SELECT * from post_cat";
	
		$result = $this->db->query($sql);
		
		if(!$result)
		{
			
		}
		
		return $result;
	}
	
	public function addPost($data)
	{
		//$data = $this->db->sanitize($data);
		$sql = "
				insert into blog_post
					set page_title = '{$data['page_title']}',
						meta_keyword = '{$data['meta_keyword']}',
						meta_desc = '{$data['meta_desc']}',
						body = '{$data['body']}',
						author = '{$data['author']}',
						title = '{$data['title']}',
						cat_id = {$data['cat_id']},
						image_url = '{$data['image_url']}'
			";
		$result = $this->db->query($sql);
	
		if(!$result)
		{
			
		}
		
		return $result;
	}
	
	public function deletePostById($id)
	{
		$id = (int)$id;
		$sql = "DELETE FROM blog_post where id = {$id}";
		$result = $this->db->query($sql);
		if(!$result)
		{
			
		}
		
		return $result;
	}
	
	public function updateDetails($data)
	{
		//$data = $this->db->sanitize($data);
		$id = (int)$data['id'];
		$sql = "
				UPDATE blog_post
					set page_title = '{$data['page_title']}',
						meta_keyword = '{$data['meta_keyword']}',
						meta_desc = '{$data['meta_desc']}',
						body = '{$data['body']}',
						author = '{$data['author']}',
						title = '{$data['title']}',
						cat_id = {$data['cat_id']},
						image_url = '{$data['image_url']}'
			 where id = {$id}";
		$result = $this->db->query($sql);
		if(!$result)
		{
			
		}
		
		return $result;
	}
	
}


?>