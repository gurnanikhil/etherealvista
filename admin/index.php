<?php
	session_start();
	include 'utils/admin_utility.php';
	$check = false;
	if(!isset($_SESSION['logged_in']))
	{
		$_SESSION['logged_in'] = 0;
	}
	
if(isset($_POST['login']))
	{
		$admin_session = new AdminSession();
		$raw_admin = $admin_session->login_admin($_POST['username'], $_POST['password']);
		if($raw_admin == -1)
		{
			echo '<h4>There has been some problem with the database. Or the Username is incorrect. Please try again.</h4>';
		}
		else if($raw_admin == -2)
		{
			$check = true;
		}
		else
		{	
			$admin = json_decode($raw_admin);
			$_SESSION['admin'] = array();
			$_SESSION['admin']['username'] = $admin->username;
			$_SESSION['admin']['name'] = $admin->name;
			$_SESSION['logged_in'] = 1;
		}
	}
?>

<!DOCTYPE html>
<html>
	<?php
		include 'utils/header.php';
	?>
	<head>
	
	</head>
<body>

<div class="container">
	<?php
	if($_SESSION['logged_in'] == 1)
	{
		?>

	<?php
		echo '<div class="jumbotron">';
			echo '<h2>Welcome back, '.$_SESSION['admin']['name'].'.</h2>';
		echo '</div>';
	}
	?>
<ul class="nav nav-tabs theme-color">
    <li class="active"><a data-toggle="tab" href="#sectionA">Section A</a></li>
    <li><a data-toggle="tab" href="#sectionB">Section B</a></li>
    <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">Dropdown <b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a data-toggle="tab" href="#dropdown1">Dropdown 1</a></li>
            <li><a data-toggle="tab" href="#dropdown2">Dropdown 2</a></li>
        </ul>
    </li>
</ul>
<div class="tab-content">
    <div id="sectionA" class="tab-pane fade in active">
        <p>Section A content…</p>
    </div>
    <div id="sectionB" class="tab-pane fade">
        <p>Section B content…</p>
    </div>
    <div id="dropdown1" class="tab-pane fade">
        <p>Dropdown 1 content…</p>
    </div>
    <div id="dropdown2" class="tab-pane fade">
        <p>Dropdown 2 content…</p>
    </div>
</div>
</div>
<script>
	$("#login").click(function(){
		$("#loginModal").modal('hide');
		$.ajax({
			type: 'POST',
			url: 'index.php',
		});
	});
	$("quotesubmit").click(function(){
		$.ajax({
		    type: 'POST',
		    url: 'quotes.php',
		})
	})	
	</script>
<script>
	$(document).ready(function(){
		$('#loginModal').modal('show');
	});

</script>
</body>
</html>