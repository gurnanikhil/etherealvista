<?php
session_start();
include 'utils/db.class.php';
include 'utils/blogPost.class.php';


?>
<!DOCTYPE HTML>
<html>
<body>
<?php   
if(isset($_SESSION['logged_in']))
{
	include 'utils/header.php';
	$blog = new Blog();
	$postData = $blog->fetchAllPosts();
?>
	<div class="container" id="showQuotes">
		<div class="container">
			<div class="panel panel-info">
				<div class="panel-heading">
				<a href="addBlog.php" style="float:right;"><button class="btn btn-sm btn-success">Add Article</button></a>
					<h4>Blog Articles</h4>
					
				</div>
				<div class="panel-body">
					<table class="table table-hover" border="">
							<tr>
								<thead class="thead thead-light">
									<th>Title</th>
									<th>Author</th>
									<th>Category</th>
									<th></th>
								</thead>
							</tr>
						<?php
						foreach($postData as $pData)
							{
						?>
						<tr>
							<td><?=$pData['title']?></td>
							<td><?=$pData['author']?></td>
							<td><?php echo $blog->getCategoryById($pData['cat_id'])[0]['name'];?></td>
							<td align="right">
								<a href="operatePost.php?func=edit&&id=<?=$pData['id']?>">
									<button class="btn btn-sm btn-primary">
										edit
									</button>
								</a>	
								<a href="operatePost.php?func=del&&id=<?=$pData['id']?>">
									<button class="btn btn-sm btn-warning">
										delete
									</button>
								</a>
							</td>
						
						</tr>
						
						<?php
							}
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
	
<?php
}
?>
</body>
</html>

<?php
if(isset($_POST['update']))
{
	$id = $_POST['update'];
	if(isset($_POST['include']))
	{
		$dbobj = new mainDB();
		$check = $dbobj->updateQuote($id, 1);
		if($check)
		{
			echo '<meta http-equiv="refresh" content="0">';
		}
	}
	else
	{
		$dbobj = new mainDB();
		$check = $dbobj->updateQuote($id, 0);
		if($check)
		{
			echo '<meta http-equiv="refresh" content="0">';
		}
	}
}
?>

<?php
if(isset($_POST['delete']))
{
	$id = $_POST['delete'];
	$dbobj = new mainDB();
	$check = $dbobj->deleteQuote($id);
	if($check)
	{
		echo '<meta http-equiv="refresh" content="0">';
	}
	else
	{
		echo 'The quote could not be deleted because of DB Error.';
	}
}
?>