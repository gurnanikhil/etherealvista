<?php

class adminUtility
{
	public function addNewImage()
	{
		if(isset($_FILES))
		{
			$description_1 = $_POST['description'];
			
			$target_short = "../images/short-images/";
			$target_medium = "../images/medium-images/";
			$target_large = "../images/large-images/";

			$targetfile_1 = $target_short.basename($_FILES['file_1']['name']);
			$targetfile_2 = $target_medium.basename($_FILES['file_2']['name']);
			$targetfile_3 = $target_large.basename($_FILES['file_3']['name']);

			move_uploaded_file($_FILES['file_1']['tmp_name'], $targetfile_1);
			move_uploaded_file($_FILES['file_2']['tmp_name'], $targetfile_2);
			move_uploaded_file($_FILES['file_3']['tmp_name'], $targetfile_3);

			$dbobj = new mainDB();
			$id = $dbobj->addNewProject($description_1);

			$val_1 = $dbobj->addNewImage($targetfile_1, 1, $id);
			$val_2 = $dbobj->addNewImage($targetfile_2, 2, $id);
			$val_3 = $dbobj->addNewImage($targetfile_3, 3, $id);

			if(isset($id) && isset($val_1) && isset($val_2) && isset($val_3))
				  echo '<h4 class="container">New Images have been added Successfully.</h4>';
		}
	}
	public function addNewQuote()
	{
		$quote = $_POST['quote'];
		$author = $_POST['author'];
		if(isset($_POST['selected']))
			$selected = 1;
		else
			$selected = 0;
		
		$dbobj = new mainDB();
		$val = $dbobj->addNewQuote($quote, $author, $selected);
		return $val;        //returns the row
	}
}

?>