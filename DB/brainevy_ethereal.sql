-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 19, 2017 at 12:22 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brainevy_ethereal`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(500) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `name`) VALUES
(1, 'nikhil', '$2y$10$QbG8CzahUDKLCdC4lasmzekfd.s1geA0oukH/YVrfbFCxKVT.1W1C', 'Nikhil Gurnani'),
(2, 'raghav', '$2y$10$QbG8CzahUDKLCdC4lasmzekfd.s1geA0oukH/YVrfbFCxKVT.1W1C', 'Raghav Tayal'),
(3, 'ragini', '$2y$10$SG6vzMHY69m8CXsT.z6El.BxHM6qo0Y/HYPt2jUMTW8XCmDtOOC/.', 'Ragini Vaid');

-- --------------------------------------------------------

--
-- Table structure for table `blog_post`
--

CREATE TABLE `blog_post` (
  `id` int(20) NOT NULL,
  `page_title` varchar(500) NOT NULL,
  `meta_keyword` varchar(500) NOT NULL,
  `meta_desc` varchar(1000) NOT NULL,
  `body` text NOT NULL,
  `author` varchar(100) NOT NULL,
  `title` varchar(500) NOT NULL,
  `cat_id` int(20) NOT NULL,
  `image_url` varchar(500) NOT NULL,
  `views` int(100) NOT NULL DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_post`
--

INSERT INTO `blog_post` (`id`, `page_title`, `meta_keyword`, `meta_desc`, `body`, `author`, `title`, `cat_id`, `image_url`, `views`, `timestamp`) VALUES
(6, 'Wordpress.org vs. Wordpress.com ', 'wordpress, CMS, content management system, blog platform', '    Wordpress.org and Wordpress.com. What is the difference between the two? Which one is better? Itâ€™s been a hot topic for a long time. Letâ€™s find out! ', '<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">Wordpress offers two primary services to its users through Wordpress.org and Wordpress.com.</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">But what is the difference between the two? Which one is better? It&rsquo;s been a hot topic for a long time. &nbsp;Let&rsquo;s find out!</span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">&nbsp; &nbsp; - &nbsp;Wordpress.org is self-hosting, hosting that you have to pay for monthly/annually; </span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;while wordpress.com offers you free hosting (up to a 3GB limit).</span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">&nbsp; &nbsp; - &nbsp;Wordpress.org involves downloading the software and installing it on your webserver.</span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">&nbsp; &nbsp; - &nbsp;Wordpress.com involves you to sign up, creating your profile for hosting your website or &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; blog.</span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">&nbsp; &nbsp; - &nbsp;Wordpress.org gives user the full control of the website whereas in Wordpress.com, the user can control the content and the design but not the whole website, rest is handled by the Wordpress technical team.</span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">&nbsp; &nbsp; - &nbsp;Wordpress.org offers different plugins and wide range of themes for your use, you can even develop a theme of your own. Wordpress.org is quite limited in this respect you only get limited themes which you can use.</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">In the end, we would like to mention that it completely depends on your necessity&nbsp;which one should you pick. If you are just looking to start a personal blog then go for wordpress.com otherwise if you are looking for a business website go for wordpress.org.</span></span></span></p>\r\n', 'Ankita Kapoor', 'Wordpress.org vs. Wordpress.com | Which one is better?', 1, '../post_images/photo_1487096097.jpg', 5, '2017-02-14 18:15:41'),
(7, 'HOW CAN SSL CERTIFICATE SECURE YOUR WEBSITE?', 'ssl, ssl certificate, website security, hacking prevention, protecting website', '    The ultimate key to the security of your website: SSL Encryption. Get over HTTP, HTTPS is the need of the hour. SSL Certificate when installed, provides a padlock along with https protocol that represents a secure connection between the client browser and the web server hosting that particular website.', '<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>The ultimate key to the security of your website: SSL Encryption.</strong></span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">Get over HTTP, HTTPS is the need of the hour. HTTPS has also been given green signal from Google, as Google states that a site which is SSL secured will have an added advantage when it comes to the site&rsquo;s page ranking.</span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><strong><span style="font-family:Times New Roman,Times,serif">What is an SSL Certificate?</span></strong></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">SSL Certificate when installed, provides a padlock along with https protocol that represents a secure connection between the client browser and the web server hosting that particular website.</span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><strong><span style="font-family:Times New Roman,Times,serif">Why is SSL required?</span></strong></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">SSL is essentially used for social media websites involving login details such as username and password, most importantly during bank transactions involving credit card, debit card, net banking etc.</span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">SSL limits the visibility of sensitive data only to the server and thus protecting it from being misused.</span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">SSL is a very good method to protect your data since your data travels across internet via different networks and nodes to reach the destination node. So SSL helps in order to protect your data from being corrupted by any of these intermediate nodes and thus your data is safely delivered to the destination.</span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">SSL uses public key cryptography using the concepts of public and private keys to protect data. Pubic key of a server, which is available for public can be used to encrypt the data sent to that server, and this encrypted data then can only be decrypted by that server via a private key.</span></span></span></p>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">SSL also protects your website against <em>phishing attacks</em> which is an added advantage. Phishing attacks happen through an email where the attacker impersonates your website but with your website being SSL secured, it reduces the chances of a phishing attack to a great extent.</span></span></span></p>\r\n', 'Ankita Kapoor', 'HOW CAN SSL CERTIFICATE SECURE YOUR WEBSITE?', 1, '../post_images/photo_1487501041.jpg', 3, '2017-02-14 18:46:54'),
(8, 'Reasons for a higher bounce rate of website', 'bounce rate, website traffic, website conversions, generate leads, engaging content', ' Why websites have higher bounce rate? Some websites tend to have higher bounce rate and thus are unable to perform well in the organicÂ search results. ', '<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><strong><span style="font-family:Calibri">Some websites tend to have higher bounce rate and thus are unable to perform well in the organic&nbsp;search results. Let&#39;s find out the reasons for such higher bounce rates -</span></strong></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><strong><span style="font-family:Calibri">1. A slow loading time</span></strong></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><span style="font-family:Calibri">People are impatient, and if your website doesn&rsquo;t load quickly enough, they&rsquo;ll abandon it and take their business elsewhere.</span></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><strong><span style="font-family:Calibri">2. Poor design and usability</span></strong></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><span style="font-family:Calibri">If your website isn&rsquo;t easy to read and easy to navigate people will abandon it in search of a website which makes sense to them. Make sure that your navigation bar is easy to use and that the layout is clean, uncluttered and any text is easy to read.</span></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><strong><span style="font-family:Calibri">3. It doesn&rsquo;t pass the &ldquo;5 second rule&rdquo;</span></strong></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><span style="font-family:Calibri">Make sure that within 5 seconds of landing on your page, your visitors know exactly what it is you do.</span></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><span style="font-family:Calibri">Financial and corporate businesses in-particular tend to flunk the 5 second rule and should pay close attention to this point. Always keep it simple and straightforward and avoid buzzwords.</span></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><span style="font-family:Calibri">For example, you&rsquo;d never catch a baker&rsquo;s website saying &ldquo;We synergise flour, yeast and salt and expose it to a dynamic, heat-based environmental paradigm shift in order to create a transformational improvement in edible carbohydrates&rdquo; as no one would have any idea what they&rsquo;re talking about. &ldquo;We bake bread the nation loves&rdquo; is fine.</span></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><strong><span style="font-family:Calibri">4. No calls to action</span></strong></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><span style="font-family:Calibri">If your website offers lots of different products or services, or is particularly large, people can easily become overwhelmed by your complicated navigation menu. Make it easier for them by guiding them to where they should be going.</span></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><strong><span style="font-family:Calibri">5. It isn&rsquo;t mobile responsive.</span></strong></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="background-color:white"><span style="font-family:Calibri">An increasingly large percentage of your traffic will be accessing your website via their phones or tablets. Make sure that your website is either mobile responsive, or very easy to use whilst on a mobile device.</span></span></span></span></p>\r\n\r\n<p style="margin-left:0in; margin-right:0in"><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Calibri">By rectifying all these issues&nbsp;</span><span style="font-family:Calibri">you can expect&nbsp;lower bounce rate.</span></span></span></p>\r\n', 'Aman Sharma', 'Reasons for a higher bounce rate of website', 1, '../post_images/photo_1487098901.jpg', 2, '2017-02-14 19:02:19'),
(9, 'Important Google analytics metrics ', 'google analytics, google analytics metric, data analysis and metric, website traffic analysis', '  Google Analytics is one of the most important tools to measure your websiteâ€™s performance and your marketing campaignâ€™s success. We provide you with the key metrics that every marketer must use for analysing purpose.', '<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">Google Analytics is one of the most important tools to measure your website&rsquo;s performance and your marketing campaign&rsquo;s success. We provide you with the key metrics that every marketer must use for analysing purpose.</span></span></span></p>\r\n\r\n<ul>\r\n	<li><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif"><strong>Bounce Rate :</strong></span>&nbsp;&nbsp;I<span style="font-family:Times New Roman,Times,serif">t reflects how many people visited a particular page but left within a few seconds without further exploring the website. Bounces are mostly just a single page visit. Therefore, it is recommended that you have a lower bounce rate.</span></span></span></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><span style="font-size:22px"><span style="color:#000000"><strong><span style="font-family:Times New Roman,Times,serif">Users :</span></strong>&nbsp;<span style="font-family:Times New Roman,Times,serif">Total number of visitors who have visited the website at least once in a session.</span></span></span></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><span style="font-size:22px"><span style="color:#000000"><strong><span style="font-family:Times New Roman,Times,serif">Page view :</span></strong>&nbsp;<span style="font-family:Times New Roman,Times,serif">How many times website pages have been viewed by visitors. High page view means high quality and value of our site&rsquo;s content.</span></span></span></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><span style="font-size:22px"><span style="color:#000000"><strong><span style="font-family:Times New Roman,Times,serif">Session (a.k.a Visits) :</span></strong>&nbsp;<span style="font-family:Times New Roman,Times,serif">Number of interactions a user has while actively engaged.</span></span></span></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul>\r\n	<li><span style="font-size:22px"><span style="color:#000000"><strong><span style="font-family:Times New Roman,Times,serif">New Session :</span></strong>&nbsp;<span style="font-family:Times New Roman,Times,serif">First time visits, it provides you information about the number of new visitors that are interested in your website.</span></span></span></li>\r\n</ul>\r\n\r\n<p><span style="font-size:22px"><span style="color:#000000"><span style="font-family:Times New Roman,Times,serif">Make sure you include&nbsp;these crucial metrics in your reports as they provide valuable information.&nbsp;</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Aman Sharma', 'Important Google analytics metrics ', 1, '../post_images/photo_1487500595.jpg', 5, '2017-02-14 19:14:04'),
(10, 'Top free digital marketing tools', 'digital marketing tools, free digital marketing tools, top online marketing tools', ' Every marketer needs access to tools that will help with digital marketing, we provide top free marketing tools that every marketer can use.', '<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Times New Roman,Times,serif">We have enlisted the top most marketing tools to help your digital marketing campaign in case you are on a tight budget or have no money to spend on marketing at all. You can use the following tools to help you with a successful marketing -</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Times New Roman,Times,serif"><strong>Buffer</strong> &ndash; social media management application designed to manage social media accounts- Facebook, Instagram, Twitter, Google+ etc. You can schedule posts by providing time and date when you want that post to be published. It is a great help to digital marketers all over the world since it saves a lot of their time.</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Times New Roman,Times,serif"><strong>Google analytics</strong> &ndash; website analysis and tracking</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Times New Roman,Times,serif">One of the top and most sought after marketing tool. Why? Firstly its provided by Google itself. Second of all, its free! What more could you ask for? It helps in keeping track of your multiple websites by providing important information about your website&rsquo;s traffic, where visitors are coming from, total number of sessions, bounce rate etc.</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Times New Roman,Times,serif"><strong>Google webmasters</strong> &ndash; website analysis and tracking</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Times New Roman,Times,serif">Again, Google product and free, this one gets into more technicalities when it comes to your website, providing information such as broken links, performance of each page of your website, ranking etc. While Google analytics gives more information regarding how successful your digital campaign is, and its impact on your website, Google webmasters gives information about your website.</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Times New Roman,Times,serif"><strong>Marketing Grader</strong> &ndash;</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Times New Roman,Times,serif">It&rsquo;s a free tool provided by Hubspot which helps with on page seo of your website. &nbsp;It provides information based on your website&rsquo;s loading time, whether its responsive or not, and most importantly where your website lacks so that you can make the required changes.</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Times New Roman,Times,serif"><strong>Moz </strong>&ndash;</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Times New Roman,Times,serif">Moz provides information regarding the backlinks which are pointing to your website. This is a very important information when it comes to getting to know your SEO ranking.</span></span></span></p>\r\n', 'Ankita Kapoor', 'Top free digital marketing tools', 1, '../post_images/photo_1487100175.jpg', 2, '2017-02-14 19:23:34'),
(11, 'Static or dynamic website? ', 'static website, dynamic website, website design, website development, blog design, blog website, portfolio website', '  Static or dynamic website? Which one should I go for? Static websites are used for blog, resume websites, business website should be made using dynamic design. ', '<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Helvetica">STATIC WEBSITE DESIGN -</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Helvetica">A static web page is a web page that is delivered to the user exactly as stored. Static websites provide super efficient, extremely fast and are usually free to host. Static web pages are suitable for the contents that never or rarely need to be updated. </span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia">They only consist of HTML, CSS, Javascript.</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia">They use no serverside language such as - PHP, Ruby, Python, Java, .NET, </span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia">A static website doesn&#39;t use a database.</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia">Static websites are not entirely obsolete today. People still use them as basic landing pages, &ldquo;coming soon&rdquo; pages, very simple brochure type websites or just an announcement page.</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">Advantages:</span></strong></span></span></p>\r\n\r\n<ol>\r\n	<li><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">Speed</span></strong><span style="font-family:Georgia"> - web servers are very good at delivering static pages quickly.</span></span></span>&nbsp;</li>\r\n	<li><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">Version control</span></strong><span style="font-family:Georgia"> - having a repository where people can collaboratively work on a project is a big win.</span></span></span></li>\r\n	<li>&nbsp;<span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">Security</span></strong><span style="font-family:Georgia"> - with no database and no processing of scripts, there&#39;s not much to mess up.</span></span></span></li>\r\n	<li><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">Maintenance</span></strong><span style="font-family:Georgia"> - there&#39;s no packages, libraries, modules, frameworks and caching systems to maintain. So the cost is almost zero.</span></span></span></li>\r\n	<li><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">Traffic surges</span></strong><span style="font-family:Georgia"> - unexpected traffic peaks is less of a problem as static HTML pages consumes a very small amount of server resources.</span></span></span></li>\r\n	<li><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">SEO</span></strong><span style="font-family:Georgia"> - Search engines like fast web pages.</span></span></span></li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">Disadvantages:</span></strong></span></span></p>\r\n\r\n<ol>\r\n	<li><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">No real-time content</span></strong><span style="font-family:Georgia"> - you lose the ability to have real-time data, but you can work around it with client side Javascript.</span></span></span></li>\r\n	<li><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">No user input</span></strong><span style="font-family:Georgia"> - for example, you can&#39;t append user comments to a blog.&nbsp;</span></span></span></li>\r\n	<li><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">No admin UI</span></strong><span style="font-family:Georgia"> - most static website generators do not have a WYSIWYG editor, you manage it in a console window such as Terminal.</span></span></span></li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Helvetica">DYNAMIC WEBSITE DESIGN &ndash;</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Helvetica">A dynamic website is a webpage that displays different content each time you visit it. On a dynamic Web page, the user can make requests (often through a form) for data contained in a database on the server that will be assembled on the fly according to what is requested. These sites require backend and database to function. Dynamic websites are preferred when your user has to interact with the website.</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Helvetica">â€‹â€‹â€‹â€‹â€‹â€‹â€‹</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">Advantages:</span></strong></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia"><strong>&nbsp; &nbsp; &nbsp;- &nbsp;User focused website</strong> - focus is on user and making everything easier for them.</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia">&nbsp; &nbsp; &nbsp; -&nbsp;&nbsp;<strong>Easy to update</strong> - since data is dynamically retrieved from the database, it can be easily modified whenever needed by the user.</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia">&nbsp; &nbsp; &nbsp; - &nbsp;<strong>Quickly convertible to responsive</strong> - these websites are responisve and hence mobile friendly for users.&nbsp;</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia"><strong>&nbsp; &nbsp; &nbsp; - &nbsp;Excellent navigation</strong> - navigation plays a key role when it comes to how easy it is for users to access website.</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia"><strong>&nbsp; &nbsp; &nbsp; - &nbsp;SEO friendly</strong> - data updated on a regular basis is good fodder for search engines and thus help websites in ranking high in different search engines.&nbsp;</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><strong><span style="font-family:Georgia">Disadvantages:</span></strong></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia"><strong>&nbsp; &nbsp; &nbsp;- &nbsp;Cost</strong> - For smaller firms Dynamic web site might be too expensive</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia"><strong>&nbsp; &nbsp; &nbsp;- &nbsp;Development time</strong> - Slightly longer initial development time</span></span></span></p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Georgia"><strong>&nbsp; &nbsp; &nbsp;- &nbsp;Hosting</strong> - Hosting is expensive</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><span style="color:#000000"><span style="font-size:22px"><span style="font-family:Helvetica">Blogs, resumes, marketing websites and documentation are all good candidates for static websites but i</span><span style="font-family:Georgia">f you were going to build a professional website for a business today, dynamic is the way to go.</span></span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Aman Sharma', 'Static or dynamic website? Which one should I go for?', 1, '../post_images/photo_1487500842.jpg', 10, '2017-02-15 19:17:45');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(5) NOT NULL,
  `category_name` varchar(400) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`) VALUES
(1, 'Web Development'),
(2, 'Graphic Designing'),
(3, 'Digital Marketing');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE `content` (
  `id` int(5) NOT NULL,
  `content` text,
  `sub_category_id` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `content`, `sub_category_id`) VALUES
(1, '<p>Test</p>', 1),
(5, '<p>Test 2</p>', 2);

-- --------------------------------------------------------

--
-- Table structure for table `large_images`
--

CREATE TABLE `large_images` (
  `id` int(5) NOT NULL,
  `filepath` varchar(400) NOT NULL,
  `project_id` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `large_images`
--

INSERT INTO `large_images` (`id`, `filepath`, `project_id`) VALUES
(1, '../images/large-images/portfolio-image-expanded-1.jpg', 19),
(2, '../images/large-images/portfolio-image-expanded-2.jpg', 20),
(3, '../images/large-images/portfolio-image-expanded-14.jpg', 21),
(4, '../images/large-images/portfolio-image-expanded-13.jpg', 22),
(7, '../images/large-images/portfolio-image-expanded-10.jpg', 25),
(8, '../images/large-images/portfolio-image-expanded-9.jpg', 26),
(9, '../images/large-images/portfolio-image-expanded-8.jpg', 27),
(10, '../images/large-images/portfolio-image-expanded-7.jpg', 28),
(12, '../images/large-images/portfolio-image-expanded-5.jpg', 30),
(13, '../images/large-images/portfolio-image-expanded-4.jpg', 31),
(14, '../images/large-images/portfolio-image-expanded-3.jpg', 32),
(15, '../images/large-images/portfolio-image-expanded-12.jpg', 33),
(16, '../images/large-images/portfolio-image-expanded-11.jpg', 34),
(17, '../images/large-images/portfolio-image-expanded-15.jpg', 35),
(18, '../images/large-images/portfolio-image-expanded-16.jpg', 36);

-- --------------------------------------------------------

--
-- Table structure for table `medium_images`
--

CREATE TABLE `medium_images` (
  `id` int(5) NOT NULL,
  `filepath` varchar(100) NOT NULL,
  `project_id` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `medium_images`
--

INSERT INTO `medium_images` (`id`, `filepath`, `project_id`) VALUES
(1, '../images/medium-images/portfolio-image-portfolio-thumbnail-1.jpg', 19),
(2, '../images/medium-images/portfolio-image-portfolio-thumbnail-2.jpg', 20),
(3, '../images/medium-images/portfolio-image-portfolio-thumbnail-14.jpg', 21),
(4, '../images/medium-images/portfolio-image-portfolio-thumbnail-13.jpg', 22),
(7, '../images/medium-images/portfolio-image-portfolio-thumbnail-10.jpg', 25),
(8, '../images/medium-images/portfolio-image-portfolio-thumbnail-9.jpg', 26),
(9, '../images/medium-images/portfolio-image-portfolio-thumbnail-8.jpg', 27),
(10, '../images/medium-images/portfolio-image-portfolio-thumbnail-7.jpg', 28),
(12, '../images/medium-images/portfolio-image-portfolio-thumbnail-5.jpg', 30),
(13, '../images/medium-images/portfolio-image-portfolio-thumbnail-4.jpg', 31),
(14, '../images/medium-images/portfolio-image-portfolio-thumbnail-3.jpg', 32),
(15, '../images/medium-images/portfolio-image-portfolio-thumbnail-12.jpg', 33),
(16, '../images/medium-images/portfolio-image-portfolio-thumbnail-11.jpg', 34),
(17, '../images/medium-images/portfolio-image-portfolio-thumbnail-15.jpg', 35),
(18, '../images/medium-images/portfolio-image-portfolio-thumbnail-16.jpg', 36);

-- --------------------------------------------------------

--
-- Table structure for table `post_cat`
--

CREATE TABLE `post_cat` (
  `id` int(20) NOT NULL,
  `name` varchar(500) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_cat`
--

INSERT INTO `post_cat` (`id`, `name`, `description`) VALUES
(1, 'cat1', 'awefwefawe'),
(2, 'cat2', 'sadsdv');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `id` int(5) NOT NULL,
  `project_name` varchar(50) NOT NULL,
  `categories_id` int(5) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `project_name`, `categories_id`) VALUES
(19, 'Kim-electricals-logo', 2),
(20, 'Face Painting Compition', 2),
(21, 'Eyesnvision', 1),
(22, 'Aardvarkplastics', 1),
(25, 'Websity Computers Visiting Card Blue', 2),
(26, 'Visiting Card Websity Computers', 2),
(27, 'N S C 2016 Coming Up', 2),
(28, 'Tractate-Paper Presentation', 2),
(30, 'Students Convention 2015', 2),
(31, 'Certificates', 2),
(32, 'Alumni Meet', 2),
(33, 'BrainyBox Logo', 2),
(34, 'Vickymarnaturals', 1),
(35, 'Desert Raiders', 2),
(36, 'Ethereal Vista Visiting Card', 2),
(40, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `query`
--

CREATE TABLE `query` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(70) NOT NULL,
  `query` text NOT NULL,
  `last_reply` text NOT NULL,
  `query_state_id` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `query`
--

INSERT INTO `query` (`id`, `name`, `email`, `query`, `last_reply`, `query_state_id`) VALUES
(7, 'Kapal Bharti', 'kappu@gmail.com', 'Kya aapke toothpaste mein namak hai?', 'Answer changed', 3),
(8, '--', '--', '--', '', 1),
(9, ';', ';', ';', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `query_state`
--

CREATE TABLE `query_state` (
  `id` int(5) NOT NULL,
  `state` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `query_state`
--

INSERT INTO `query_state` (`id`, `state`) VALUES
(1, 'Pending'),
(2, 'Solved'),
(3, 'In Progress');

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE `quotes` (
  `id` int(20) NOT NULL,
  `quote` text NOT NULL,
  `author` varchar(60) NOT NULL,
  `selected` int(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `quote`, `author`, `selected`) VALUES
(1, 'U cen tek mai car, but u cent tek mai sweg.', 'Dolan', 1);

-- --------------------------------------------------------

--
-- Table structure for table `small_images`
--

CREATE TABLE `small_images` (
  `id` int(5) NOT NULL,
  `filepath` varchar(100) NOT NULL,
  `project_id` int(5) NOT NULL,
  `include` int(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `small_images`
--

INSERT INTO `small_images` (`id`, `filepath`, `project_id`, `include`) VALUES
(1, '../images/short-images/portfolio-image-small-thumbnail-1.jpg', 19, 1),
(2, '../images/short-images/portfolio-image-small-thumbnail-2.jpg', 20, 1),
(3, '../images/short-images/portfolio-image-small-thumbnail-14.jpg', 21, 1),
(4, '../images/short-images/portfolio-image-small-thumbnail-13.jpg', 22, 1),
(7, '../images/short-images/portfolio-image-small-thumbnail-10.jpg', 25, 1),
(8, '../images/short-images/portfolio-image-small-thumbnail-9.jpg', 26, 1),
(9, '../images/short-images/portfolio-image-small-thumbnail-8.jpg', 27, 1),
(10, '../images/short-images/portfolio-image-small-thumbnail-7.jpg', 28, 1),
(12, '../images/short-images/portfolio-image-small-thumbnail-5.jpg', 30, 1),
(13, '../images/short-images/portfolio-image-small-thumbnail-4.jpg', 31, 1),
(14, '../images/short-images/portfolio-image-small-thumbnail-3.jpg', 32, 1),
(15, '../images/short-images/portfolio-image-small-thumbnail-12.jpg', 33, 1),
(16, '../images/short-images/portfolio-image-small-thumbnail-11.jpg', 34, 1),
(17, '../images/short-images/portfolio-image-small-thumbnail-15.jpg', 35, 1),
(18, '../images/short-images/portfolio-image-small-thumbnail-16.jpg', 36, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` int(5) NOT NULL,
  `email` varchar(80) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`, `status`, `timestamp`) VALUES
(44, 'raghav.tayal93@gmail.com', 1, '2017-01-17 14:56:56'),
(45, 'raghav.tayal1993@gmail.com', 1, '2017-01-17 14:59:31'),
(46, 'raghav.tayal1993@gmail.com', 1, '2017-01-25 13:38:49');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(5) NOT NULL,
  `sub_category_name` varchar(500) NOT NULL,
  `categories_id` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `sub_category_name`, `categories_id`) VALUES
(1, 'E - Commerce', 1),
(2, 'Content Management System', 1),
(3, 'Entertainment', 1),
(4, 'Web Portal', 1),
(5, 'Informational', 1),
(6, 'Portfolio/Personal', 1),
(7, 'Photography', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_post`
--
ALTER TABLE `blog_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content`
--
ALTER TABLE `content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_category_id` (`sub_category_id`);

--
-- Indexes for table `large_images`
--
ALTER TABLE `large_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `medium_images`
--
ALTER TABLE `medium_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `post_cat`
--
ALTER TABLE `post_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_id` (`categories_id`),
  ADD KEY `categories_id_2` (`categories_id`);

--
-- Indexes for table `query`
--
ALTER TABLE `query`
  ADD PRIMARY KEY (`id`),
  ADD KEY `query_state_id` (`query_state_id`);

--
-- Indexes for table `query_state`
--
ALTER TABLE `query_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `small_images`
--
ALTER TABLE `small_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_id` (`categories_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `blog_post`
--
ALTER TABLE `blog_post`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `large_images`
--
ALTER TABLE `large_images`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `medium_images`
--
ALTER TABLE `medium_images`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `post_cat`
--
ALTER TABLE `post_cat`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `query`
--
ALTER TABLE `query`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `query_state`
--
ALTER TABLE `query_state`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `small_images`
--
ALTER TABLE `small_images`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `content`
--
ALTER TABLE `content`
  ADD CONSTRAINT `content_ibfk_1` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`);

--
-- Constraints for table `large_images`
--
ALTER TABLE `large_images`
  ADD CONSTRAINT `large_images_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `medium_images`
--
ALTER TABLE `medium_images`
  ADD CONSTRAINT `medium_images_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `project`
--
ALTER TABLE `project`
  ADD CONSTRAINT `project_ibfk_1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `query`
--
ALTER TABLE `query`
  ADD CONSTRAINT `query_ibfk_1` FOREIGN KEY (`query_state_id`) REFERENCES `query_state` (`id`);

--
-- Constraints for table `small_images`
--
ALTER TABLE `small_images`
  ADD CONSTRAINT `small_images_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `project` (`id`);

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_ibfk_1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
