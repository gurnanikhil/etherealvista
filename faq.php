<!DOCTYPE HTML>
<html lang="en" class="no-js">
<head>
		<title>FAQ | Ethereal Vista</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="Team Ethereal Vista">
		<meta name="description" content="Ethereal Vista based in Delhi, provides services in website design, website development, graphic design and digital marketing.">
		<meta name="keywords" content="website design company,website development company,graphic design company, digital marketing company">
<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
<!--End meta info-->
<!--Start stylesheet include-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">

<!--End stylesheet include-->
<!--Start scripts include-->
<script type="text/javascript" src="js/modernizr.js"></script>
<!--End scripts include-->

<link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">

<style>
dl
{
font-size: 18px;
}
</style>
</head>
<body>
<!--Start preloader-->
<div id="preloader">
<img src="images/loader-big.gif" alt="Please Wait">
</div>
<!--End preloader-->
<!--Start header-->
<?php
include 'utils-ui/navbar.php';
?>
<!--End header-->
<!--Start main section-->
<section class="main secondary_page" data-animate-up="header-static" data-animate-down="header-small">
<!--Start title container-->
<div class="title_container type_3 t_align_center">
<div class="container">
<ul class="path_list clearfix">
<li><a href="index.php">Home</a></li>
<li><i></i><a href="faq.php">FAQ</a></li>
</ul>
<h1>FAQ Page</h1>
</div>
</div>
<section class="faq_page_padding">

<div class="container">

<!--End faq navigation block-->
<main class="dd_container">
<div class="container">
<div class="accordion_type_01 faq" id="web_design">
	<dl class="accordion_group active">
		<dt>
			What does Ethereal Vista mean?
			<span class="icon">
				<span class="hex_elem_rounded"></span>
				<span class="hex_elem_rounded"><span>&#43;</span><span>&minus;</span></span>
			</span>
		</dt>
		<dd>
			<p>Honestly? Ethereal Vista sounds really cool, that is why we used it. Jokes apart, it means a sharp/pleasent view that grabs your attention in the best possible way.</p>
		</dd>
	</dl>
	<dl class="accordion_group">
		<dt>
			What is Ethereal Vista?
			<span class="icon">
				<span class="hex_elem_rounded"></span>
				<span class="hex_elem_rounded"><span>&#43;</span><span>&minus;</span></span>
			</span>
		</dt>
		<dd>
			Ethereal Vista is a start up organisation Co-Founded by some college students. It is aimed at providing a way so that anyone/everyone can have their own website, logos and whatnot. We are a bunch of Designers and Developers, with the aim of creating High Quality websites and graphics at the best possible price with complete satisfaction of the customers.
		</dd>
	</dl>
	<dl class="accordion_group">
		<dt>
			I am new to all this, can you explain how it all works?
			<span class="icon">
				<span class="hex_elem_rounded"></span>
				<span class="hex_elem_rounded"><span>&#43;</span><span>&minus;</span></span>
			</span>
		</dt>
		<dd>
			<p>We at Ethereal Vista believe in a special and individual treatment. So we realise if you are new to get a website or any graphic work, we ensure that our process is as transparent and understandable as it can be. So that you get what you desire, the way you desire.<br>
			The process is simple. In order to ensure that you get your deserved treatment, head over to our <a style="color:blue;" href="contact.php">Contact Us</a> page and drop us a message with your name and email. Rest assured, we'll get to you as earliest as possible.</p>
		</dd>
	</dl>
	<dl class="accordion_group">
		<dt>
			What all do you design?
			<span class="icon">
				<span class="hex_elem_rounded"></span>
				<span class="hex_elem_rounded"><span>&#43;</span><span>&minus;</span></span>
			</span>
		</dt>
		<dd>
			 We specialize in Graphics and Web Designing. For more information on our specialisation, <a style="color:blue;" href="our_services.php">Click here</a>.
		</dd>
	</dl>
	<dl class="accordion_group">
		<dt>
			What are your payments/charges?
			<span class="icon">
				<span class="hex_elem_rounded"></span>
				<span class="hex_elem_rounded"><span>&#43;</span><span>&minus;</span></span>
			</span>
		</dt>
		<dd>
			Due to business reasons, the Price list is not available online. You can reach us at our <a style="color:blue;" href="mailto:support@etherealvista.com">Support Email</a> or send us a message via the <a style="color:blue;" href="contact.php">Contact Us</a> page.
		</dd>
	</dl>
	<dl class="accordion_group">
		<dt>
			Can I see some of your work?
			<span class="icon">
				<span class="hex_elem_rounded"></span>
				<span class="hex_elem_rounded"><span>&#43;</span><span>&minus;</span></span>
			</span>
		</dt>
		<dd>
			<p>You sure can, <a style="color:blue;" href="portfolio.php">Click here</a>.</p>
		</dd>
	</dl>
	<dl class="accordion_group">
		<dt>
			How do you take payment?
			<span class="icon">
				<span class="hex_elem_rounded"></span>
				<span class="hex_elem_rounded"><span>&#43;</span><span>&minus;</span></span>
			</span>
		</dt>
		<dd>
			We have a very clear 2 step payment process. At the time of finalisation of the requirements and the structure, you'll have to pay 30-40% (depending on the size) of the total development cost in advance. And since, we like to be transparent with the customers, we'll give you the receipt and updates on your order regularly. 
			After the completion of the project, we deliver the final product to you and you can then transfer the remaining amount after you are satisfied. Neat, innit?
		</dd>
	</dl>
</div>
</div>
</main>
</div>
</section>
</section>
<!--End main section-->
<!--Start footer-->
<?php
	include 'utils-ui/footer.php';
?>
<!--End footer-->
<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/jquery.hashchange.min.js"></script>
		<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80174278-2', 'auto');
  ga('send', 'pageview');

</script>		
</body>
</html>