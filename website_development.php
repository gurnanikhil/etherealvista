<!DOCTYPE HTML>
<html lang="en" class="no-js">

<head>
<title>Website Development services | ETHEREAL VISTA</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="Team Ethereal Vista">
		<meta name="description" content="Ethereal Vista is a leading website development company based in Delhi NCR.">
		<meta name="keywords" content="website development company,ecommerce development,web design company,ecommerce development company Delhi">
<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
<!--End meta info-->
<!--Start stylesheet include-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">

<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css">
<!--End stylesheet include-->
<!--Start scripts include-->
<script type="text/javascript" src="js/modernizr.js"></script>
<!--End scripts include-->

<link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">

</head>
<body>
<!--Start preloader-->
<div id="preloader">
	<img src="images/loader-big.gif" alt="Please Wait">
</div>
<!--End preloader-->
<!--Start header-->
<?php
include 'utils-ui/navbar.php';
?>
<!--End header-->
<!--Start main section-->
<section class="main secondary_page blog_default" data-animate-up="header-static" data-animate-down="header-small">
<!--Start title container-->
<div class="title_container type_3 t_align_center">
	<div class="container">
		<a class="prev_page" href="website_design.php">Website <br>Design</a>
		<a class="next_page" href="graphic_design.php">Graphic <br>Design</a>
		<h1>Website<br> Development</h1>
	</div>
</div>
<!--End title container-->
<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/services_ecommerce.jpg" alt="eCommerce">
				<a href="images/services_ecommerce.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">5+ </div>
					<div>Done</div>
				</div>
				<h3>Ecommerce Development<br></h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>These websites exist on a buyer-seller relationship. A buyer can directly place order to a seller through these websites, track his order, cancel orders, initiate returns. Basically everything that you can do with normal commerce. </p>
			<!--End post content-->
			
		</article>
	</div>
</section>
<!--End article container-->
<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/services_cms.jpg" alt="CMS">
				<a href="images/services_cms.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">11+ </div>
					<div>Done</div>
				</div>
				<h3>Content Mananagement Systems</h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>CMS are majorly used for publishing, editing and modifying content of a site. Wordpress is the most famous CMS currently in the market, providing efficient plugins. </p>
			<!--End post content-->
		</article>
	</div>
</section>
<!--End article container-->

<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/service_entertaintment.jpg" alt="Entertainment Website">
				<a href="images/service_entertaintment.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">9+ </div>
					<div>Done</div>
				</div>
				<h3>Entertainment Website Development</h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>Website for the entertainment industry, engaging the audiences with breathtaking designs and equipped with effective social media. </p>
			<!--End post content-->
			
		</article>
	</div>
</section>
<!--End article container-->
<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/service_web_portal.jpg" alt="Web Portal Development">
				<a href="images/service_web_portal.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">2+ </div>
					<div>Done</div>
				</div>
				<h3>Web Portal Development</h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>A web site that combines information coming from diverse sources. Each information source gets its dedicated area on the page for displaying information, the user can configure which ones to display. </p>
			<!--End post content-->
		</article>
	</div>
</section>
<!--End article container-->
			<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/services_info_website.jpg" alt="Informal Website">
				<a href="images/services_info_website.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">25+ </div>
					<div>Done</div>
				</div>
				<h3>Informational Website Development<br></h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>The very first website on Internet were Informational Websites. These provide information of a particular topic, sometimes commonly known as Blogs. A Blog is a kind of Informational Website, consisting of posts which are displayed in reverse chronological order. </p>
			<!--End post content-->
			
		</article>
	</div>
</section>
<!--End article container-->

<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/services_personal_website.jpg" alt="Portfolio Website">
				<a href="images/services_personal_website.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">29+ </div>
					<div>Done</div>
				</div>
				<h3>Portfolio/Personal Website Development</h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>An individual's website, providing them a platform to showcase their skills and ideas through visuals, photographs and content. </p>
			<!--End post content-->
			
		</article>
	</div>
</section>
<!--End article container-->

			
<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/photo-new.jpg" alt="Photography Website">
				<a href="images/photo-new.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">3+ </div>
					<div>Done</div>
				</div>
				<h3>Photography Website Development</h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>An online portfolio for photographers customized according to their needs and design expectations. Great for photograph enthusiasts, letting them share photographs online. </p>
			<!--End post content-->
			
		</article>
	</div>
</section>
<!--End article container-->
</section>
<!--End main section-->
<!--Start footer-->
<?php
include 'utils-ui/footer.php';
?>
<!--End footer-->
<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/jquery.hashchange.min.js"></script>
		<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80174278-2', 'auto');
  ga('send', 'pageview');

</script>		
</body>
</html>