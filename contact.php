<!DOCTYPE HTML>
<html lang="en" class="no-js">

<head>
		<title>Contact Us | ETHEREAL VISTA</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="Team Ethereal Vista">
		<meta name="description" content="Ethereal Vista based in Delhi, provides services in website design, website development, graphic design and digital marketing.">
		<meta name="keywords" content="website design company,website development company,graphic design company,web solutions,ecommerce development company,seo services company">
<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
<!--End meta info-->
<!--Start stylesheet include-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">

<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css">
<!--End stylesheet include-->
<!--Start scripts include-->
<script type="text/javascript" src="js/modernizr.js"></script>
<!--End scripts include-->

<link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">

<style>
::-webkit-input-placeholder {
color: #C2E8E7;
}

:-moz-placeholder { /* Firefox 18- */
color: #C2E8E7;  
}

::-moz-placeholder {  /* Firefox 19+ */
color: #C2E8E7;  
}

:-ms-input-placeholder {  
color: #C2E8E7;  
}

</style>		
</head>
<body>
<!--Start preloader-->
<div id="preloader">
<img src="images/loader-big.gif" alt="Please Wait">
</div>
<!--End preloader-->
<!--Start header-->
<?php
include 'utils-ui/navbar.php';
?>
<!--End header-->
<!--Start main section-->
<section class="main secondary_page contact" data-animate-up="header-static" data-animate-down="header-small">
<!--Start title container-->
<div class="title_container t_align_center type_3">
<div class="container">
<ul class="path_list clearfix">
<li><a href="index.php">Home</a></li>
<li><i></i><a href="contact.php">Contact Us</a></li>
</ul>
<h1>Contact Us</h1>
</div>
</div>
<!--End title container-->
	<!--
		<div class="gmap_container">
		<div id="gmap"></div>
		<div class="cd_container">
			<div class="cd_shadow"></div>
			<div id="company_description" class="hex_elem_rounded_type_2">
				<span>
					<img src="images/contact_logo.png" alt="Contact Us">
					<img src="images/contact_logo_02.png" alt="Contact Us">
					<h5>2989, 1st Floor Ram bazar, <br>
						Mori Gate, Delhi-110006<br>
						India
					</h5>
					<span> +91-8527488515<br>support@etherealvista.com</span>
				</span>
				<span class="h_el_01"></span>
				<span class="h_el_02"></span>
			</div>
		</div>
		</div>-->
<div class="flip_container">
<!--Start contact hexagons-->
<section class="contact_hexagons">
	<div class="container o_hide clearfix">
		<!--Start contact info hexagon-->
		<div class="hex_elem_rounded_type_2 contact_info f_left">
			<div>
				<div class="hex_elem_rounded_type_2 c_icon_m">
					<span class="h_el_01"></span>
					<span class="h_el_02"></span>
					<span class="ic"></span>
				</div>
				<h5>Main Branch</h5>
				<p>2989, 1st Floor Ram bazar, Mori Gate<br> Delhi-110006<br> India</p>
			</div>
			<span class="h_el_01"></span>
			<span class="h_el_02"></span>
		</div>
		<!--End contact info hexagon-->
		<!--Start button for open contact form-->
		<div class="hex_elem_rounded_type_2 open_contact_form f_left">
			<div class="sm_button">
				<h5>Send Message</h5>
			</div>
			<div class="hex_elem_rounded_type_2 icon">
				<img src="images/icons_type_03_mail.png" alt="Write To Us">
				<span class="h_el_01"></span>
				<span class="h_el_02"></span>
			</div>
			<span class="h_el_01"></span>
			<span class="h_el_02"></span>
		</div>
		<!--End button for open contact form-->
		<!--Start contact info hexagon-->
		<div class="hex_elem_rounded_type_2 contact_info f_left">
			<div>
				<div class="hex_elem_rounded_type_2 c_icon_h">
					<span class="h_el_01"></span>
					<span class="h_el_02"></span>
					<span class="ic"></span>
				</div>
				<h5>Contact Details</h5>
				<ul>
					<li>Phone: +91-8527488515</li>
					<li>Email: support@etherealvista.com</li>
				</ul>
			</div>
			<span class="h_el_01"></span>
			<span class="h_el_02"></span>
		</div>
		<!--End contact info hexagon-->
	</div>
</section>
<!--End contact hexagons-->
<!--Start contact form section-->
<section class="contact_form_section">
	<div class="container relative">
		<form id="contact_form" action="" method="post">
			<img src="images/icons_type_03_mail.png" alt="Write To Us">
			<input required="required" type="text" placeholder="Full Name" name="uname" style="width:45%; background-color:#3EB5B1;border-bottom:1px solid #99D3CE;font-size:23px;color:#B7E3E2;" />
			<input required="required" type="text" placeholder="Email ID" name="umail" style="width:45%; background-color:#3EB5B1;border-bottom:1px solid #99D3CE;font-size:23px;color:#B7E3E2;float:right;" />
			<textarea required="required" name="message">Ask a question</textarea>
			<button type="submit" name="submitquery">
				<span>Send <br> Message</span>
				<span class="hex_elem_rounded_type_2">
					<span class="h_el_01"></span>
					<span class="h_el_02"></span>
				</span>
			</button>
		</form>
		<button class="close_form">&#215;</button>
	</div>
</section>
<!--End contact form section-->
</div>
</section>
<!--End main section-->
<!--Start footer-->
<?php
include 'utils-ui/footer.php';
?>
<!--End footer-->
<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
		
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80174278-2', 'auto');
  ga('send', 'pageview');

</script>		
</body>
</html>

<?php
include 'utils/database.php';
if(isset($_POST['submitquery']))
{
if(isset($_POST['uname']) && isset($_POST['umail']))
{
$db = new mainDB();
$db->connect();
$errcheck = $db->contactus();
if($errcheck == 0)
{
echo '<head><script> alert("Thank you for submitting query. We\'ll be returning back to you shortly."); </script></head>';
}
else if($errcheck == -1)
{
die('<head><script> alert("Sorry, some error has occurred at our end. We regret any inconvenience caused."); </script></head>');
}
}
}
unset($nameholder, $mailholder, $errcheck)
?>