<!DOCTYPE HTML>
<html lang="en" class="no-js">
<head>
		<title>Our Team - PROSPECT</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
		<!--End meta info-->
		<!--Start stylesheet include-->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
		<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">
		
		<!--End stylesheet include-->
		<!--Start scripts include-->
		<script type="text/javascript" src="js/modernizr.js"></script>
		<!--End scripts include-->
	</head>
	<body>
		<!--Start preloader-->
			<div id="preloader">
				<img src="images/loader-big.gif" alt="">
			</div>
		<!--End preloader-->
		<!--Start header-->
		<?php
			include 'utils-ui/navbar.php';
		?>
		<!--End header-->
		<!--Start main section-->
		<section class="main secondary_page" data-animate-up="header-static" data-animate-down="header-small">
			<!--Start title container-->
			<div class="title_container type_3 t_align_center">
				<div class="container">
					<a class="prev_page" href="pages_timeline.html">TimeLine</a>
					<a class="next_page" href="pages_process.html">Process</a>
					<h1>Our Team</h1>
				</div>
			</div>
			<section class="grey_text_color page_padding our_team_c">
				<div class="container teams_counts">
					<div class="row">
						<div class="span4">
							<ul class="faq_nav our_team">
								<li>
									<a class="hex_elem_rounded_type_2">
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
										<span class="hex_elem_rounded_type_2 with_border">
											<span class="h_el_01"></span>
											<span class="h_el_02"></span>
											<span class="faq_title">
												<span class="icon-edit"></span>
												Web Design
											</span>
										</span>
										<span class="counting_tape">5</span>
										<span class="stripe"></span>
										<span class="circle"></span>
									</a>
								</li>
								<li>
									<a class="hex_elem_rounded_type_2">
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
										<span class="hex_elem_rounded_type_2 with_border">
											<span class="h_el_01"></span>
											<span class="h_el_02"></span>
											<span class="faq_title">
												<span class="icon-headphones"></span>
												Support Teams
											</span>
										</span>
										<span class="counting_tape">10</span>
										<span class="stripe"></span>
										<span class="circle"></span>
									</a>
								</li>
							</ul>
						</div>
						<div class="span4 offset4">
							<ul class="faq_nav our_team">
								<li>
									<a class="hex_elem_rounded_type_2">
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
										<span class="hex_elem_rounded_type_2 with_border">
											<span class="h_el_01"></span>
											<span class="h_el_02"></span>
											<span class="faq_title">
												<span class="icon-wrench"></span>
												Web Developers
											</span>
										</span>
										<span class="counting_tape">11</span>
										<span class="stripe"></span>
										<span class="circle"></span>
									</a>
								</li>
								<li>
									<a class="hex_elem_rounded_type_2">
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
										<span class="hex_elem_rounded_type_2 with_border">
											<span class="h_el_01"></span>
											<span class="h_el_02"></span>
											<span class="faq_title">
												<span class="icon-suitcase"></span>
												Financial Officers
											</span>
										</span>
										<span class="counting_tape">9</span>
										<span class="stripe"></span>
										<span class="circle"></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="container relative">
					<div class="our_team_title_wrap">
						<div class="our_team_title"><div></div></div>
						<div class="content">
							<div class="title_item web_dev">
								<img src="images/our_team_icon_02.png" alt="">
								<h1>Web Developer</h1>
								<p class="ot_title_shape m_top_15">
								<!--Start span's for the text to wrap the shape-->
								<span></span><span></span><span></span><span></span><span></span><span></span>
								<span></span><span></span>
								<!--End div's for the text to wrap the shape-->
								Sed interdum. Proin tristique, lorem quis scelerisque facilisis, quam magna rutrum orci, eu aliqu ibero lorem sit amet</p>
							</div>
							<div class="title_item support_team">
								<img src="images/our_team_icon_02.png" alt="">
								<h1>Support Teams</h1>
								<p class="ot_title_shape m_top_15">
								<!--Start span's for the text to wrap the shape-->
								<span></span><span></span><span></span><span></span><span></span><span></span>
								<span></span><span></span>
								<!--End div's for the text to wrap the shape-->
								Sed interdum. Proin tristique, lorem quis scelerisque facilisis, quam magna rutrum orci, eu aliqu ibero lorem sit amet</p>
							</div>
							<div class="title_item game_dev">
								<img src="images/our_team_icon_02.png" alt="">
								<h1>Game Dev</h1>
								<p class="ot_title_shape m_top_15">
								<!--Start span's for the text to wrap the shape-->
								<span></span><span></span><span></span><span></span><span></span><span></span>
								<span></span><span></span>
								<!--End div's for the text to wrap the shape-->
								Sed interdum. Proin tristique, lorem quis scelerisque facilisis, quam magna rutrum orci, eu aliqu ibero lorem sit amet</p>
							</div>
							<ul class="inline clearfix">
								<button class="our_team_prev f_left"></button>
								<button class="our_team_next f_left	"></button>
							</ul>
						</div>
					</div>
				</div>
				<!--Start our team section-->
					<section class="our_team_container clearfix parallax">
						<ul class="our_team_carousel">
							<li data-item="web_dev">
								<span class="container relative clearfix">
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_01">
											<img src="images/our_team_img_04.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<h6>Mick Neil</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
										</div>
										<!--End team item description-->
									</figure>
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_02">
											<img src="images/our_team_img_03.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
											<h6>Mick Mars</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
											<span></span>
										</div>
										<!--End team item description-->
									</figure>
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_01">
											<img src="images/our_team_img_03.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<h6>Vince Lee</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
											<span></span>
										</div>
										<!--End team item description-->
									</figure>
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_02">
											<img src="images/our_team_img_04.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
											<h6>Andrea Stephens</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
										</div>
										<!--End team item description-->
									</figure>
									<!--Start advertising-->
									<figure class="team_advertising">
										<div class="triangle"></div>
										<span>
											<img src="images/our_team_twitter_icon.png" alt="">
											Follow Us On Twitter
										</span>
									</figure>
									<!--End advertising-->
								</span>		
							</li>
							<li data-item="support_team">
								<span class="container relative clearfix">
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_01">
											<img src="images/our_team_img_03.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<h6>Mick Neil</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
										</div>
										<!--End team item description-->
									</figure>
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_02">
											<img src="images/our_team_img_04.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
											<h6>Mick Mars</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
											<span></span>
										</div>
										<!--End team item description-->
									</figure>
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_01">
											<img src="images/our_team_img_04.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<h6>Vince Lee</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
											<span></span>
										</div>
										<!--End team item description-->
									</figure>
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_02">
											<img src="images/our_team_img_03.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
											<h6>Andrea Stephens</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
										</div>
										<!--End team item description-->
									</figure>
									<!--Start advertising-->
									<figure class="team_advertising">
										<div class="triangle"></div>
										<span>
											<img src="images/our_team_twitter_icon.png" alt="">
											Follow Us On Twitter
										</span>
									</figure>
									<!--End advertising-->
								</span>		
							</li>
							<li data-item="game_dev">
								<span class="container relative clearfix">
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_01">
											<img src="images/our_team_img_04.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<h6>Mick Neil</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
											<span></span>
										</div>
										<!--End team item description-->
									</figure>
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_02">
											<img src="images/our_team_img_03.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
											<h6>Mick Mars</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
											<span></span>
										</div>
										<!--End team item description-->
									</figure>
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_01">
											<img src="images/our_team_img_03.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<h6>Vince Lee</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
											<span></span>
										</div>
										<!--End team item description-->
									</figure>
									<figure class="team_item clearfix">
										<!--Start team item photo-->
										<div class="triangle_container_type_02">
											<img src="images/our_team_img_04.jpg" alt="">
											<canvas width="389" height="269"></canvas>
										</div>
										<!--End team item photo-->
										<!--Start team item description-->
										<div class="team_item_description">
											<p>Mauris euismod, velit non facilisis sollicitudin, orci libero vehicula sem, a sagittis dui</p>
											<h6>Andrea Stephens</h6>
											<div class="color_scheme bold">&minus; Developer &minus;</div>
											<span></span>
										</div>
										<!--End team item description-->
									</figure>
									<!--Start advertising-->
									<figure class="team_advertising">
										<div class="triangle"></div>
										<span>
											<img src="images/our_team_twitter_icon.png" alt="">
											Follow Us On Twitter
										</span>
									</figure>
									<!--End advertising-->
								</span>		
							</li>
						</ul>
					</section>
				<!--End our team section-->
				<section class="m_top_40 t_align_center">
					<div class="container">
						<h1 class="default">Want Work With Us?</h1>
						<p>Integer sed vulputate ligula. Suspendisse lacinia convallis metus nec egestas.<br>
						Ut pellentesque ipsum eu orci ornare, in congue urna laoree</p>
						<!--Start social icons-->
							<ul class="social_icons_type_03 inline clearfix">
								<li class="twitter">
									<a href="#" class="hex_elem_rounded_type_2">
										<span class="s_icon"></span>
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
									</a>
								</li>
								<li class="rss">
									<a href="#" class="hex_elem_rounded_type_2">
										<span class="s_icon"></span>
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
									</a>
								</li>
								<li class="facebook">
									<a href="#" class="hex_elem_rounded_type_2">
										<span class="s_icon"></span>
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
									</a>
								</li>
								<li class="yahoo">
									<a href="#" class="hex_elem_rounded_type_2">
										<span class="s_icon"></span>
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
									</a>
								</li>
								<li class="stumbleupon">
									<a href="#" class="hex_elem_rounded_type_2">
										<span class="s_icon"></span>
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
									</a>
								</li>
							</ul>
							<!--End social icons-->
					</div>
				</section>
			</section>
		</section>
		<!--End main section-->
		<!--Start footer-->
		<?php 
			include 'utils-ui/footer.php';
		?>
		<!--End footer-->
		<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/jquery.carouFredSel-6.0.3-packed.js"></script>
		<script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
	</body>
</html>