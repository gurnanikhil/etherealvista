<!DOCTYPE HTML>
<html lang="en" class="no-js">

<head>
		<title>Graphic Design services|ETHEREAL VISTA</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="Team Ethereal Vista">
		<meta name="description" content="Ethereal Vista is a leading website designing company based in Delhi NCR.">
		<meta name="keywords" content="graphic design company,website design company,website development company,">
<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
<!--End meta info-->
<!--Start stylesheet include-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">

<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css">
<!--End stylesheet include-->
<!--Start scripts include-->
<script type="text/javascript" src="js/modernizr.js"></script>
<!--End scripts include-->

<link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">

</head>
<body>
<!--Start preloader-->
<div id="preloader">
	<img src="images/loader-big.gif" alt="Please Wait">
</div>
<!--End preloader-->
<!--Start header-->
<?php
include 'utils-ui/navbar.php';
?>
<!--End header-->
<!--Start main section-->
<section class="main secondary_page blog_default" data-animate-up="header-static" data-animate-down="header-small">
<!--Start title container-->
<div class="title_container type_3 t_align_center">
	<div class="container">
		<a class="prev_page" href="website_development.php"><br>Website Development</a>
		<a class="next_page" href="digital_marketing.php">Digital Marketing</a>
		<h1>Graphic Design</h1>
	</div>
</div>
<!--End title container-->
<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/services_logo.jpg" alt="Logos">
				<a href="images/services_logo.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">15+ </div>
					<div>Done</div>
				</div>
				<h3>Logo Design<br><br></h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>Logos are like first impressions, your first step towards Branding. An impressive and eye catching logo does half of your job for you. </p>
			<!--End post content-->
			
		</article>
	</div>
</section>
<!--End article container-->
<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/services_poster_design.jpg" alt="Poster Design">
				<a href="images/services_poster_design.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">11+ </div>
					<div>Done</div>
				</div>
				<h3>Poster Design<br><br></h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>Although posters are used for multiple purposes; their key role is to provide the desired information through appealing graphics and texts. Commonly used by advertisers,artists, and any one trying to communicate a message. </p>
			<!--End post content-->
		</article>
	</div>
</section>
<!--End article container-->

<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/services_brochure_design.jpg" alt="Brochure Design">
				<a href="images/services_brochure_design.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">29+ </div>
					<div>Done</div>
				</div>
				<h3>Brochure<br> Design<br></h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>A brochure is an introduction of your company to the potential clients.It not only describes your work but also promotes it, thus giving you a chance to turn your potential clients into your regular customers. </p>
			<!--End post content-->
			
		</article>
	</div>
</section>
<!--End article container-->
<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/services_stationery-packaging.jpg" alt="Stationary">
				<a href="images/services_stationery-packaging.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">6+ </div>
					<div>Done</div>
				</div>
				<h3>Stationery Designing</h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>A product is the core of a company, and how it appears to the masses is one of its biggest selling point.A stunning design is just what you need for a succesfull launch of your product. It also plays a key role in rebranding. </p>
			<!--End post content-->
		</article>
	</div>
</section>
<!--End article container-->
			<!--Start article container-->
<section class="article_container">
	<div class="container">
		<article>
			<!--Start image-->
			<div class="colorbox_container f_left">
				<img src="images/service_billboards.jpg" alt="Advertising">
				<a href="images/service_billboards.jpg" data-rel="prettyPhoto[blogdeafult]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<div class="colorbox_inner_overlay"></div>
			</div>
			<!--Start post info (date , title) -->
			<div class="post_info">
				<div class="date_container hex_elem_rounded f_left">
					<div align="center">26+ </div>
					<div>Done</div>
				</div>
				<h3>Advertising<br><br></h3>
			</div>
			<!--End post info (date , title) -->
			<!--Start post content-->
			<p>Advertising on websites is done through videos, audios, texts and images.It includes Banner designing, Billboard designing and many other such formats. </p>
			<!--End post content-->
			
		</article>
	</div>
</section>
<!--End article container-->

</section>
<!--End main section-->
<!--Start footer-->
<?php
include 'utils-ui/footer.php';
?>
<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/jquery.hashchange.min.js"></script>
		<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80174278-2', 'auto');
  ga('send', 'pageview');

</script>		
</body>
</html>