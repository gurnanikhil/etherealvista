<?php
	$path = $_SERVER['REQUEST_URI'];
	$filename = basename($path, '.php');
?>

<header class="header" >
			<div class="container">
				<!--Start logo-->
				<a class="logo f_left" href="index.php" title="Prospect">
					<img src="images/logo.png" alt="">
				</a>
				<!--End logo-->
				<!--Start main menu-->
				<button class="menu_button clearfix">
					<img class="r_logo" src="images/r_menu_logo.png" alt="">
					<img class="r_button" src="images/r_menu_button.png" alt="">
				</button>
				<ul class="f_right main_menu">
					<li <?php if($filename == 'index') echo 'class="current_item"';?>>
						<a href="index.php">
							<span class="hex_elem_rounded"><i class="icon-home"></i></span>
							Home
						</a>
					</li>
					<li <?php if($filename == 'about_us') echo 'class="current_item"';?>>
						<a href="about_us.php">
							<span class="hex_elem_rounded"><i class="icon-cog"></i></span>
							About Us
						</a>
					</li>
					<li <?php if($filename == 'our_services') echo 'class="current_item"';?>>
						<a href="#">
							<span class="hex_elem_rounded"><i class="icon-file-alt"></i></span>
							Services
						</a>
							
						<!--Start pages submenu-->
							<ul>
								<li><a href="our_services.php">Overview</a></li>
								<li><a href="website_design.php">Website Design</a></li>
								<li><a href="website_development.php">Website Development</a></li>
								<li><a href="graphic_design.php">Graphic Design</a></li>
								<li><a href="digital_marketing.php">Digital Marketing</a></li>
							</ul>
						<!--End pages submenu-->
					</li>
					<li <?php if($filename == 'portfolio') echo 'class="current_item"';?>>
						<a href="portfolio.php">
							<span class="hex_elem_rounded"><i class="icon-picture"></i></span>
							Portfolio
						</a>
					</li>
					<li <?php if($filename == 'blog') echo 'class="current_item"';?>>
						<a href="blog.php">
							<span class="hex_elem_rounded"><i class="icon-comments"></i></span>
							Blog
						</a>
					</li>
					<li <?php if($filename == 'faq') echo 'class="current_item"';?> >
						<a href="faq.php">
							<span class="hex_elem_rounded"><i class="icon-comments"></i></span>
							FAQ
						</a>
					</li>
					<li <?php if($filename == 'contact') echo 'class="current_item"';?> >
						<a href="contact.php">
							<span class="hex_elem_rounded"><i class="icon-envelope"></i></span>
							Contact
						</a>
					</li>
				</ul>
				<!--End main menu-->
			</div>
		</header>