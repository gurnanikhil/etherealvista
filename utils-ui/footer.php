<footer class="footer parallax">
<div class="footer_cornice">
<div class="container">
<div class="relative">
<div class="skew_elem_01"></div>
<div class="skew_elem_02"></div>
</div>
</div>
</div>
<div class="container">
<div class="row footer_inner">

<div class="span3 offset2_5 t_align_right">
<figure>
	<figcaption>
		<h5>Ethereal Vista</h5>
	</figcaption>
	<p><span class="white">Ethereal Vista</span> has been launched with a clear vision of providing quality services to its customers by giving utmost importance to customer's satisfaction.</p>
</figure>
<hr class="divider_type_01">

</div>
<div class="span1 t_align_center">
<div class="footer_logo_container">
	<img src="images/footer_logo.png" alt="Ethereal Vista">
</div>	
</div>
<div class="span3">
<figure>
	<figcaption>
		<h5>Contact Info</h5> 
	</figcaption>
	<ul class="address_list">
		<li class="address">2989, 1st Floor Ram bazar, <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mori Gate, Delhi-110006<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;India</li>
		<li class="phone"> +91-8527488515</li>
		<li class="mail"><a href="mailto:#"></a>support@etherealvista.com</li>
		
	</ul>
</figure>
<hr class="divider_type_01">

</div>
<p style="clear:both;"></p>
<div align="center">
<ul class="social_icons_type_01" style="margin-top:-10px;">
	<li>
		<a target="_blank" href="https://twitter.com/ethereal_vista">
			<div class="default"><i class="icon-twitter"></i></div>
			<div class="active">
				<div class="clean_cornice"><span></span><span></span><span></span></div>
				<div class="clean_cornice_2"></div>
				<i class="icon-twitter"></i>
			</div>
		</a>
	</li>
	<li>
		<a target="_blank" href="https://www.facebook.com/Ethereal-Vista-223083851416366">
			<div class="default"><i class="icon-facebook"></i></div>
			<div class="active">
				<div class="clean_cornice"><span></span><span></span><span></span></div>
				<div class="clean_cornice_2"></div>
				<i class="icon-facebook"></i>
			</div>
		</a>
	</li>
	<li>
		<a target="_blank" href="https://www.instagram.com/ethereal_vista/">
			<div class="default"><i class="icon-instagram"></i></div>
			<div class="active">
				<div class="clean_cornice"><span></span><span></span><span></span></div>
				<div class="clean_cornice_2"></div>
				<i class="icon-instagram"></i>
			</div>
		</a>
	</li>
	<li>
		<a href="#">
			<div class="default"><i class="icon-google-plus"></i></div>
			<div class="active">
				<div class="clean_cornice"><span></span><span></span><span></span></div>
				<div class="clean_cornice_2"></div>
				<i class="icon-google-plus"></i>
			</div>
		</a>
	</li>
</ul>
</div>	
</div>
</div>
<div class="copyright_container t_align_center">
<div class="container">
<a href="https://www.etherealvista.com/">Ethereal Vista</a> &copy; Copyright 2016, All Rights Reserved
</div>
</div>
<a class="scrolltop color_hexagon">
<span></span>
<span></span>
<span></span>
</a>
</footer>