<!DOCTYPE HTML>
<html lang="en" class="no-js">

<head>
<title>Digital Marketing Services|ETHEREAL VISTA</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="Team Ethereal Vista">
		<meta name="description" content="Ethereal Vista is a leading digital marketing services providing company based in Delhi.">
		<meta name="keywords" content="seo services Delhi,search engine optimization company,display marketing company,content writing,email marketing services">
<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
<!--End meta info-->
<!--Start stylesheet include-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">

<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css">
<!--End stylesheet include-->
<!--Start scripts include-->
<script type="text/javascript" src="js/modernizr.js"></script>
<!--End scripts include-->

<link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">

</head>
<body>
<!--Start preloader-->
	<div id="preloader">
		<img src="images/loader-big.gif" alt="Please Wait">
	</div>
<!--End preloader-->
<!--Start header-->
<?php
	include 'utils-ui/navbar.php';
?>
<!--End header-->
<!--Start main section-->
<section class="main secondary_page blog_default" data-animate-up="header-static" data-animate-down="header-small">
	<!--Start title container-->
	<div class="title_container type_3 t_align_center">
		<div class="container">
			<a class="prev_page" href="graphic_design.php">Graphic Design</a>
			<h1>Digital Marketing</h1>
		</div>
	</div>
	<!--End title container-->
	<!--Start article container-->
	<section class="article_container">
		<div class="container">
			<article>
				<!--Start image-->
				<div class="colorbox_container f_left">
					<img src="images/service_digital_marketing.jpg" alt="Digital Marketing">
					<a href="images/service_digital_marketing.jpg" data-rel="prettyPhoto[blogdeafult]">
						<span class="active_icon"></span>
						<span class="colorscheme_icon"></span>
					</a>
					<div class="colorbox_inner_overlay"></div>
				</div>
				<!--Start post info (date , title) -->
				<div class="post_info">
					<div class="date_container hex_elem_rounded f_left">
						<div align="center">11+ </div>
						<div>Done</div>
					</div>
					<h3>SEO<br><br></h3>
				</div>
				<!--End post info (date , title) -->
				<!--Start post content-->
				<p>Search Engine Optimization is for improving the visibility of your website on the internet. It helps you in reaching your target audience, increasing the traffic and improving your website's rankings in popular search engines such as Yahoo, Google and Bing. </p>
				<!--End post content-->
				
			</article>
		</div>
	</section>
	<!--End article container-->
	<!--Start article container-->
	<section class="article_container">
		<div class="container">
			<article>
				<!--Start image-->
				<div class="colorbox_container f_left">
					<img src="images/service_content.jpg" alt="Content Writing">
					<a href="images/service_content.jpg" data-rel="prettyPhoto[blogdeafult]">
						<span class="active_icon"></span>
						<span class="colorscheme_icon"></span>
					</a>
					<div class="colorbox_inner_overlay"></div>
				</div>
				<!--Start post info (date , title) -->
				<div class="post_info">
					<div class="date_container hex_elem_rounded f_left">
						<div align="center">21+ </div>
						<div>Done</div>
					</div>
					<h3>Content Writing<br><br></h3>
				</div>
				<!--End post info (date , title) -->
				<!--Start post content-->
				<p>The right words at the right place make the perfect impact. Content plays a crucial role in getting the point across for the company. It provides a clear understanding of the product to the customer. </p>
				<!--End post content-->
			</article>
		</div>
	</section>
	<!--End article container-->
	
	<!--Start article container-->
	<section class="article_container">
		<div class="container">
			<article>
				<!--Start image-->
				<div class="colorbox_container f_left">
					<img src="images/service_social_media_marketing.jpg" alt="Social Media Marketing">
					<a href="images/service_social_media_marketing.jpg" data-rel="prettyPhoto[blogdeafult]">
						<span class="active_icon"></span>
						<span class="colorscheme_icon"></span>
					</a>
					<div class="colorbox_inner_overlay"></div>
				</div>
				<!--Start post info (date , title) -->
				<div class="post_info">
					<div class="date_container hex_elem_rounded f_left">
						<div align="center">9+ </div>
						<div>Done</div>
					</div>
					<h3>Social Media Marketing</h3>
				</div>
				<!--End post info (date , title) -->
				<!--Start post content-->
				<p>This involves making use of various social media platforms like Facebook, Twitter, Instagram and LinkedIn, to promote your product. Staying connected with the world, to transform your visitors into customers, social media is your best bet. Account setup and managing,branding and designing, editorial work, social media advertising are all part of this. </p>
				<!--End post content-->
				
			</article>
		</div>
	</section>
	<!--End article container-->
	<!--Start article container-->
	<section class="article_container">
		<div class="container">
			<article>
				<!--Start image-->
				<div class="colorbox_container f_left">
					<img src="images/service_email_marketing.jpg" alt="Email Marketing">
					<a href="images/service_email_marketing.jpg" data-rel="prettyPhoto[blogdeafult]">
						<span class="active_icon"></span>
						<span class="colorscheme_icon"></span>
					</a>
					<div class="colorbox_inner_overlay"></div>
				</div>
				<!--Start post info (date , title) -->
				<div class="post_info">
					<div class="date_container hex_elem_rounded f_left">
						<div align="center">6+ </div>
						<div>Done</div>
					</div>
					<h3>Email Marketing<br><br></h3>
				</div>
				<!--End post info (date , title) -->
				<!--Start post content-->
				<p>Email is one of the oldest and common way to broadcast your messages. Companies use email to communicate with their customers.Sending out relevant information to your customers through emails, such that your customers want to read it. </p>
				<!--End post content-->
			</article>
		</div>
	</section>
	<!--End article container-->
				<!--Start article container-->
	<section class="article_container">
		<div class="container">
			<article>
				<!--Start image-->
				<div class="colorbox_container f_left">
					<img src="images/services_display.jpg" alt="Display Marketing">
					<a href="images/services_display.jpg" data-rel="prettyPhoto[blogdeafult]">
						<span class="active_icon"></span>
						<span class="colorscheme_icon"></span>
					</a>
					<div class="colorbox_inner_overlay"></div>
				</div>
				<!--Start post info (date , title) -->
				<div class="post_info">
					<div class="date_container hex_elem_rounded f_left">
						<div align="center">2+ </div>
						<div>Done</div>
					</div>
					<h3>Display Marketing<br><br></h3>
				</div>
				<!--End post info (date , title) -->
				<!--Start post content-->
				<p>Animated ads and ads with audio-visual elements help in getting your point across effectively. In such a way, you can promote relevant content quite easily. </p>
				<!--End post content-->
				
			</article>
		</div>
	</section>
	<!--End article container-->
	
</section>
<!--End main section-->
<!--Start footer-->
<?php
	include 'utils-ui/footer.php';
?>
<!--End footer-->
		<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/jquery.hashchange.min.js"></script>
		<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80174278-2', 'auto');
  ga('send', 'pageview');

</script>		
</body>

</html>