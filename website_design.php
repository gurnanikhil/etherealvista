<!DOCTYPE HTML>
<html lang="en" class="no-js">

<head>
<title>Website Design services | ETHEREAL VISTA</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="Team Ethereal Vista">
		<meta name="description" content="Ethereal Vista is a leading website designing company based in Delhi NCR.">
		<meta name="keywords" content="website design company,website design services,static website,dynamic website,responsive website">
<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
<!--End meta info-->
<!--Start stylesheet include-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">

<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css">
<!--End stylesheet include-->
<!--Start scripts include-->
<script type="text/javascript" src="js/modernizr.js"></script>
<!--End scripts include-->

<link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">

</head>
<body>
<!--Start preloader-->
	<div id="preloader">
		<img src="images/loader-big.gif" alt="Please Wait">
	</div>
<!--End preloader-->
<!--Start header-->
<?php
	include 'utils-ui/navbar.php';
?>
<!--End header-->
<!--Start main section-->
<section class="main secondary_page blog_default" data-animate-up="header-static" data-animate-down="header-small">
	<!--Start title container-->
	<div class="title_container type_2 t_align_center">
		<div class="container">
			<a class="next_page" href="website_development.php">Website <br>Development</a>
			<h1>Website Design</h1>
		</div>
	</div>
	<!--End title container-->
	<!--Start article container-->
	<section class="article_container">
		<div class="container">
			<article>
				<!--Start image-->
				<div class="colorbox_container f_left">
					<img src="images/service_static_design.jpg" alt="Static Web Design">
					<a href="images/service_static_design.jpg" data-rel="prettyPhoto[blogdeafult]">
						<span class="active_icon"></span>
						<span class="colorscheme_icon"></span>
					</a>
					<div class="colorbox_inner_overlay"></div>
				</div>
				<!--Start post info (date , title) -->
				<div class="post_info">
					<div class="date_container hex_elem_rounded f_left">
						<div align="center">12+ </div>
						<div>Done</div>
					</div>
					<h3>Static Website <br>Design<br></h3>
				</div>
				<!--End post info (date , title) -->
				<!--Start post content-->
				<p>A static web page is a web page that is delivered to the user exactly as stored. Static websites provide super efficient, extremely fast and are usually free to host. Static web pages are suitable for the contents that never or rarely need to be updated. Blogs, resumes, marketing websites and documentation are all good candidates for static websites. </p>
				<!--End post content-->
				
			</article>
		</div>
	</section>
	<!--End article container-->
	<!--Start article container-->
	<section class="article_container">
		<div class="container">
			<article>
				<!--Start image-->
				<div class="colorbox_container f_left">
					<img src="images/service_dynamic_designt.jpg" alt="">
					<a href="images/service_dynamic_designt.jpg" data-rel="prettyPhoto[blogdeafult]">
						<span class="active_icon"></span>
						<span class="colorscheme_icon"></span>
					</a>
					<div class="colorbox_inner_overlay"></div>
				</div>
				<!--Start post info (date , title) -->
				<div class="post_info">
					<div class="date_container hex_elem_rounded f_left">
						<div align="center">15+ </div>
						<div>Done</div>
					</div>
					<h3>Dynamic Website Design</h3>
				</div>
				<!--End post info (date , title) -->
				<!--Start post content-->
				<p>A dynamic website is a webpage that displays different content each time you visit it. On a dynamic Web page, the user can make requests (often through a form) for data contained in a database on the server that will be assembled on the fly according to what is requested. These sites require backend and database to function. Dynamic websites are preferred when your user has to interact with the website. </p>
				<!--End post content-->
			</article>
		</div>
	</section>
	<!--End article container-->
	
	<!--Start article container-->
	<section class="article_container">
		<div class="container">
			<article>
				<!--Start image-->
				<div class="colorbox_container f_left">
					<img src="images/service_responsive.jpg" alt="Responsive Web Design">
					<a href="images/service_responsive.jpg" data-rel="prettyPhoto[blogdeafult]">
						<span class="active_icon"></span>
						<span class="colorscheme_icon"></span>
					</a>
					<div class="colorbox_inner_overlay"></div>
				</div>
				<!--Start post info (date , title) -->
				<div class="post_info">
					<div class="date_container hex_elem_rounded f_left">
						<div align="center">19+ </div>
						<div>Done</div>
					</div>
					<h3>Responsive Website Design</h3>
				</div>
				<!--End post info (date , title) -->
				<!--Start post content-->
				<p>By making a website responsive, it becomes user friendly in a way that it can respond to different devices like mobile phones,tablets,laptops and desktops. The website becomes hardware independent. </p>
				<!--End post content-->
				
			</article>
		</div>
	</section>
	<!--End article container-->
	<!--Start article container-->
	<section class="article_container">
		<div class="container">
			<article>
				<!--Start image-->
				<div class="colorbox_container f_left">
					<img src="images/services_redesign.jpg" alt="Website Redesign">
					<a href="images/services_redesign.jpg" data-rel="prettyPhoto[blogdeafult]">
						<span class="active_icon"></span>
						<span class="colorscheme_icon"></span>
					</a>
					<div class="colorbox_inner_overlay"></div>
				</div>
				<!--Start post info (date , title) -->
				<div class="post_info">
					<div class="date_container hex_elem_rounded f_left">
						<div align="center">8+ </div>
						<div>Done</div>
					</div>
					<h3>Website <br>Re Design</h3>
				</div>
				<!--End post info (date , title) -->
				<!--Start post content-->
				<p>When your current website is no longer able to fulfil its purpose for you, going for re designing suits your purpose. Its budget friendly and time efficient. </p>
				<!--End post content-->
			</article>
		</div>
	</section>
	<!--End article container-->
</section>
<!--End main section-->
<!--Start footer-->
<?php
	include 'utils-ui/footer.php';
?>
<!--End footer-->
<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/jquery.hashchange.min.js"></script>
		<script type="text/javascript" src="js/jquery.easytabs.min.js"></script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80174278-2', 'auto');
  ga('send', 'pageview');

</script>		
</body>
</html>