<?php
session_start();

include 'utils/db.class.php';
include 'utils/blogPost.class.php';

$blog = new Blog();

$id=(int)$_GET['id'];
if(isset($_GET['page']))
{
	$start=$_GET['page']*$postPerPage - $postPerPage;
	$pageNo=$_GET['page'];
}

$postData=$blog->getPostById($id);
$postData=$postData[0];

$latestPostData = $blog->fetchPosts(0,6);
$popularPostData = $blog->fetchPopularPosts();

//Updating Number of views
	$blog->updateCount($id);
?>
<!DOCTYPE HTML>
<html lang="en" class="no-js">
	
<head>
		<title><?=$postData['page_title']?></title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="<?=$postData['author']?>">
		<meta name="description" content="<?=$postData['meta_desc']?>">
		<meta name="keywords" content="<?=$postData['meta_keyword']?>">
		<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
		<!--End meta info-->
		<!--Start stylesheet include-->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
		<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">
		
		<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css">
		<!--End stylesheet include-->
		<!--Start scripts include-->
		<script type="text/javascript" src="js/modernizr.js"></script>
		<!--End scripts include-->
	</head>
	<body>
		<!--Start preloader-->
			<div id="preloader">
				<img src="images/loader-big.gif" alt="">
			</div>
		<!--End preloader-->
		<!--Start header-->
<?php
include 'utils-ui/navbar.php';
?>
		<!--End header-->
		<!--Start main section-->
		<section class="main secondary_page blog_post" data-animate-up="header-static" data-animate-down="header-small">
			<!--Start title container-->
			<div class="title_container type_2 t_align_center">
				<div class="container">
					<ul class="path_list clearfix">
						<li>Home &nbsp; <span>&#8260;</span></li>
						<li>Blog &nbsp; <span>&#8260;</span></li>
					</ul>
					<h1>Blog Post</h1>
				</div>
			</div>
			<!--End title container-->
			<!--Start article container-->
			<section class="article_container">
				<div class="container">
					<div class="row">
						<!--Start content column-->
						<section class="span9">
							<article>
								<!--Start image-->
								<div class="colorbox_container" >
								<div style="max-height:300px;">
									<img src="post_images/<?=$postData['image_url']?>" alt="" style="width:100%;">
									<a href="post_images/<?=$postData['image_url']?>" data-rel="prettyPhoto[blog]">
										<span class="active_icon"></span>
										<span class="colorscheme_icon"></span>
									</a>
									<div class="colorbox_inner_overlay"></div>
								</div>
								</div>
								<!--Start post info (date , title) -->
								<div class="post_info clearfix">
									<div class="date_container hex_elem_rounded f_left">
										<div><?=date("d/m",strtotime($postData['timestamp']))?></div>
										<div><?=date("Y",strtotime($postData['timestamp']))?></div>
									</div>
									<h3><?=$postData['title']?></h3>
									<ul class="post_tags_list clearfix f_left">
										<li>
											<a href="#">
												<div class="post_tag_icon f_left">
													<i class="icon-user"></i>
												</div>
												<?=$postData['author']?>
											</a>
										</li>
										<li>
											<a href="#">
												<div class="post_tag_icon f_left">
													<i class="icon-tag"></i>
												</div>
												<?php echo $blog->getCategoryById($postData['cat_id'])[0]['name'];?>
											</a>
										</li>
									</ul>
								</div>
								<!--End post info (date , title) -->
								<!--Start post content-->
								<div class="post_content">
									<p><?=$postData['body']?></p>
								</div>
								<!--End post content-->
							</article>
							<hr class="divider_type_02">
						</section>
						<!--End content column-->
						<!--Start sidebar-->
						<aside class="span3">
							<!--Start searchform-->
							<form method="post" role="search" class="clearfix" action="search.php">
								<input type="text" placeholder="Search" name="query">
								<input type="submit" value="">
							</form>
							<!--End searchform-->
							<!--Start social icons-->
														<ul class="social_icons_type_03 clearfix">
								<li class="twitter">
									<a href="https://twitter.com/ethereal_vista" class="hex_elem_rounded_type_2" target="_blank">
										<span class="s_icon"></span>
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
									</a>
								</li>
								<li class="facebook">
									<a href="https://www.facebook.com/Ethereal-Vista-223083851416366" class="hex_elem_rounded_type_2" target="_blank">
										<span class="s_icon"></span>
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
									</a>
								</li>
								<li class="yahoo">
									<a href="https://www.instagram.com/etherealvista/" class="hex_elem_rounded_type_2">
										<span class="s_icon"></span>
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
									</a>
								</li>
								<li class="stumbleupon">
									<a href="#" class="hex_elem_rounded_type_2">
										<span class="s_icon"></span>
										<span class="h_el_01"></span>
										<span class="h_el_02"></span>
									</a>
								</li>
							</ul>
							<!--End social icons-->
							<!--Start post categories-->
							<!--figure class="widget">
								<figcaption>
									<h4>Post Categories</h4>
								</figcaption>
								<ul class="categories_list">
									<li><a href="#"><span class="icon-angle-right"></span>Audios (2)</a></li>
									<li><a href="#"><span class="icon-angle-right"></span>Daily Inspiration (7)</a></li>
									<li><a href="#"><span class="icon-angle-right"></span>Freelance (3)</a></li>
									<li><a href="#"><span class="icon-angle-right"></span>Links (1)</a></li>
									<li><a href="#"><span class="icon-angle-right"></span>Mobile (1)</a></li>
									<li><a href="#"><span class="icon-angle-right"></span>Photography (5)</a></li>
									<li><a href="#"><span class="icon-angle-right"></span>Quotes (1)</a></li>
									<li><a href="#"><span class="icon-angle-right"></span>Resources (3)</a></li>
									<li><a href="#"><span class="icon-angle-right"></span>Status (1)</a></li>
								</ul>
							</figure-->
							<!--End post categories-->


							<!--Start latest posts-->
							<figure class="widget">
								<figcaption>
									<h4>Popular Posts</h4>
								</figcaption>
								<div class="relative">
									<div class="latest_post_nav clearfix">
										<button class="latest_post_prev"></button>
										<button class="latest_post_next"></button>
									</div>
									<ul class="latest_post_carousel">
									<?php
									//foreach( $latestPostData as $lData)
									for( $i=0;$i<sizeof($popularPostData) ;$i=$i+2)
									{
									?>
										<li>
											<article>
												<div class="hexagon_mini_container f_left">
													<img src="post_images/<?=$latestPostData[$i]['image_url']?>" alt="">
													<canvas width="89" height="100"></canvas>
												</div>
												<div class="compressed_post_content">
													<a href="blog_post.php?id=<?=$latestPostData[$i]['id']?>"><?php echo substr( strip_tags($latestPostData[$i]['title']),0,20);?></a>
													<p><?php echo substr( strip_tags($latestPostData[$i]['body']),0,100);?> </p>
													<div class="color_scheme"><?=date("M d, Y",strtotime($latestPostData[$i]['timestamp']))?></div>
												</div>
											</article>
											<?php 
											
											if($i+1 <sizeof($latestPostData))
											{
											?>
											
											<article>
												<div class="hexagon_mini_container f_left">
													<img src="post_images/<?=$latestPostData[$i+1]['image_url']?>" alt="">
													<canvas width="89" height="100"></canvas>
												</div>
												<div class="compressed_post_content">
													<a href="blog_post.php?id=<?=$latestPostData[$i]['id']?>"><?php echo substr( strip_tags($latestPostData[$i+1]['title']),0,20);?></a>
													<p><?php echo substr( strip_tags($latestPostData[$i+1]['body']),0,100);?> </p>
													<div class="color_scheme"><?=date("M d, Y",strtotime($latestPostData[$i+1]['timestamp']))?></div>
												</div>
											</article>
											<?php
											}
											?>
										</li>
										<?php
									}
										?>
									</ul>
								</div>
							</figure>
							
							
							<!--End latest posts-->
							<!--Start tags-->
							<!--figure class="widget">
								<figcaption>
									<h4>Tags</h4>
								</figcaption>
								<ul class="tags_list">
									<li><a href="#"><span></span>Art</a></li>
									<li><a href="#"><span></span>Awesome</a></li>
									<li><a href="#"><span></span>Classic</a></li>
									<li><a href="#"><span></span>Custom</a></li>
									<li><a href="#"><span></span>Data</a></li>
									<li><a href="#"><span></span>Photography</a></li>
									<li><a href="#"><span></span>Funny</a></li>
									<li><a href="#"><span></span>Gaming</a></li>
									<li><a href="#"><span></span>Tips</a></li>
									<li><a href="#"><span></span>Photography</a></li>
									<li><a href="#"><span></span>Standart</a></li>
									<li><a href="#"><span></span>Art</a></li>
								</ul>
							</figure-->
							<!--End tags-->
						</aside>
						<!--End sidebar-->
					</div>
				</div>
			</section>
			<!--End article container-->

		</section>
		<!--End main section-->
					
			<!--Start register-->	
			<section class="register_container type1 parallax">	
				<div class="container">
					<div class="inline wind">
						<h2 class="slog">NEWS LETTER</h2>
						<p style="font-size:20px;" id="message">Stay up-to date with the latest news and other stuffs,Sign Up today!</p>
					</div>
					<form id="sign-up-form" action="" method="POST" class="form_style_01 wind">
						<input type="email" id="mail_address" name="subscriber_email" style="border-color:black;" placeholder="Email address" required>
						<button type="button" id="ajax-submit" class="button_style_03_medium transparent" value="">Sign up now!
					</button>
					</form>
				</div>
			</section>
			
<!--End register-->
		<!--Start footer-->
<?php
include 'utils-ui/footer.php';
?>
		<!--End footer-->
		<!--Start scripts include-->
	<script>

      var result_message = document.getElementById("message");

      function postResult(value) {
        result_message.innerHTML = value;
		document.getElementById("sign-up-form").reset() = '';
      }

      function clearResult() {
         result_message.innerHTML = '';
      }

      function signUpFunc() {
        clearResult();

		var form = document.getElementById("sign-up-form");
        var action = "signUp.php";

        // gather form data
		var form_data = new FormData(form);
		
        var xhr = new XMLHttpRequest();
        xhr.open('POST', action, true);

        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.onreadystatechange = function () {
          if(xhr.readyState == 4 && xhr.status == 200) {
            var result = xhr.responseText;
            
            postResult(result);
          }
        };
        xhr.send(form_data);
      }

      var button = document.getElementById("ajax-submit");
      button.addEventListener("click", signUpFunc);

    </script>
		
		
		
		
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="js/kalendae.standalone.min.js"></script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/jflickrfeed.min.js"></script>
		<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="js/jquery.carouFredSel-6.0.3-packed.js"></script>
		<script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
	</body>

</html>