<?php

class DB{
	
	protected $connection;
	protected $host = "localhost";
	protected $user = "root" ;
	protected $password = "password1234";
	protected $db_name = "brainevy_ethereal";
	protected $lastId ;
	
	private static $instance = null;
	
	private function __construct()
	{

	}
	
	
	public static function getInstance()
    {
        if(self::$instance == null)
        {
            self::$instance = new DB();
        }

        return self::$instance;
    }

	
	public function openConnection()
	{
		$this->connection = new mysqli($this->host, $this->user, $this->password, $this->db_name);
		
		if(mysqli_connect_error() )
		{
			throw new Exception('Could not connect to DB');
		}
	}
	
	public function closeConnection()
	{
		if (isset($this->connection)) {
			// Close database connection
			$this->connection->close();
		}
	}
	
	public function query($sql)
	{
		$this->openConnection();
		
		if(!$this->connection)
		{
			return false;
		}
		
		$result = $this->connection->query($sql);
		$this->lastId = $this->connection->insert_id;

		if( mysqli_error($this->connection))
		{
			throw new Exception(mysqli_error($this->connection));
		}
		
		if(is_bool($result))
		{
			return $result ; 
		}
		
		$data = array();
		while( $row = mysqli_fetch_assoc($result))
		{
			$data[] = $row;
		}
		
		$this->closeConnection();
		
		return $data;
	}
	
	public function getLastId()
	{
		return $this->lastId;
	}
	
	public function escape($str)
	{
		return mysqli_escape_string($this->connection, $str);
	}
	
	function cleanInput($input) 
	{
 
		$search = array(
			'@<script[^>]*?>.*?</script>@si',   // Strip out javascript
			'@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
			'@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
			'@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
		  );
	 
		$output = preg_replace($search, '', $input);
		return $output;
	}
	
	public function sanitize($input) 
	{
		if (is_array($input)) 
		{
			foreach($input as $var=>$val) 
			{
				$output[$var] = $this->sanitize($val);
			}
		}
		else 
		{
			if (get_magic_quotes_gpc()) 
			{
				$input = stripslashes($input);
			}
			
			$input  = $this->cleanInput($input);
			$output = mysqli_real_escape_string($this->connection,$input);
		}
		return $output;
	}	
	

	
}


?>