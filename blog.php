<?php
session_start();

include 'utils/db.class.php';
include 'utils/blogPost.class.php';

$blog = new Blog();

//$postData = $blog->fetchAllPosts();
$postPerPage=3;
$pageNo=1;
$start=0;
if(isset($_GET['page']))
{
	$start=$_GET['page']*$postPerPage - $postPerPage;
	$pageNo=$_GET['page'];
}
	

$postData = $blog->fetchPosts($start,$postPerPage);
$postCount=$blog->postCount();
$postCount=$postCount[0]['postCount'];


?>
<!DOCTYPE HTML>
<html lang="en" class="no-js">
	
<head>
		<title>Blog Ethereal Vista</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
		<!--End meta info-->
		<!--Start stylesheet include-->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
		<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">
		
		<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css">
		<!--End stylesheet include-->
		<!--Start scripts include-->
		<script type="text/javascript" src="js/modernizr.js"></script>
		<!--End scripts include-->
	</head>
	<body>
		<!--Start preloader-->
			<div id="preloader">
				<img src="images/loader-big.gif" alt="">
			</div>
		<!--End preloader-->
		<!--Start header-->
<?php
include 'utils-ui/navbar.php';
?>
		<!--End header-->
		<!--Start main section-->
		<section class="main secondary_page blog_default" data-animate-up="header-static" data-animate-down="header-small">
			<!--Start title container-->
			<div class="title_container type_2 t_align_center">
				<div class="container">
					<ul class="path_list clearfix">
						<li><a href="index.php">Home</a></li>
						<li><i></i><a href="blog.php">Blog</a></li>
					</ul>
					<h1>Blog</h1>
				</div>
			</div>
			<!--End title container-->

	<?php 
	foreach($postData as $pData) 
	{
	?>
			<!--Start article container-->
			<section class="article_container">
				<div class="container">
					<article>
						<!--Start image-->
						<div class="colorbox_container f_left">
							<img src="post_images/<?=$pData['image_url']?>" alt="">
							<a href="post_images/<?=$pData['image_url']?>" data-rel="prettyPhoto[blogdeafult]">
								<span class="active_icon"></span>
								<span class="colorscheme_icon"></span>
							</a>
							<div class="colorbox_inner_overlay"></div>
						</div>
						<!--Start post info (date , title) -->
						<div class="post_info">
							<div class="date_container hex_elem_rounded f_left">
								<div><?=date("d/m",strtotime($pData['timestamp']))?></div>
								<div><?=date("Y",strtotime($pData['timestamp']))?></div>
							</div>
							<h3><?=$pData['title']?></h3>
						</div>
						<!--End post info (date , title) -->
						<!--Start post content-->
						<p><?php echo substr( strip_tags($pData['body']),0,500);?></p>
						<!--End post content-->
						<hr class="divider_type_02">
						<!--Start links for post-->
						<div class="clearfix post_tags_container">
							<ul class="post_tags_list clearfix f_left">
								<li>
									<a href="#">
										<div class="post_tag_icon f_left">
											<i class="icon-user"></i>
										</div>
										<?=$pData['author']?>
									</a>
								</li>
								<li>
									<a href="#">
										<div class="post_tag_icon f_left">
											<i class="icon-tag"></i>
										</div>
										<?php echo $blog->getCategoryById($pData['cat_id'])[0]['name'];?>
									</a>
								</li>
							</ul>
							<a href="blog_post.php?id=<?=$pData['id']?>" class="button_style_05 f_right">Read More</a>
						</div>
						<!--End link for post-->
					</article>
				</div>
			</section>
			<!--End article container-->
<?php
}
?>			
			
			<div class="container" style="padding:30px 0">
						<?php
			if($postPerPage*$pageNo < $postCount)
			{
			?>
			
							<a href="blog.php?page=<?php echo $pageNo+1;?>"><button class="button_style_03_medium thin" style="float:right;">Older Posts ></button></a>
			<?php } ?>
			<?php
			if($pageNo-1 > 0)
			{
			?>
							<a href="blog.php?page=<?php echo $pageNo-1?>"><button class="button_style_03_medium thin" style="float:left;"> < Newer Posts </button>
			<?php
			}
			?>			
			</div>
		</section>
		<!--End main section-->
		<!--Start footer-->
<?php
include 'utils-ui/footer.php';
?>
		<!--End footer-->
		<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="js/audio.min.js"></script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
	</body>

</html>