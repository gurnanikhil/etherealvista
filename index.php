<?php
include 'utils/database.php';
?>

<!DOCTYPE HTML>
<html lang="en" class="no-js">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#">

<title>ETHEREAL VISTA | Website Design &amp; Development, Graphic Design and Digital Marketing company</title> 
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="Team Ethereal Vista">
		<meta name="description" content="Ethereal Vista based in Delhi, provides website design, website development, graphic design and digital marketing services.">
		<meta name="keywords" content="website design company,website development company,graphic design, web solutions,ecommerce development company,seo services">
<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
<!--End meta info-->
<!--Start stylesheet include-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
<link rel="stylesheet" type="text/css" href="css/settings.css">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">	
<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">
<link rel="stylesheet" type="text/css" href="css/prettyPhoto.css">
<!--End stylesheet include-->
<!--Start scripts include-->
<script type="text/javascript" src="js/modernizr.js"></script>
<!--End scripts include-->


<link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">

<meta property="og:title" content="ETHEREAL VISTA | Website Design Development,Graphic Design and Digital Marketing company">
<meta property="og:url" content="http://etherealvista.com/">
<meta property="og:image" content="http://etherealvista.com/images/fbshare.jpg">
<meta property="og:image:width" content="1200">
<meta property="og:image:height" content="630">
<meta property="og:description" content="Ethereal Vista based in Delhi,provides services in website design,website development,graphic design and digital marketing.">
<meta property="og:site_name" content="EtherealVista.com">

</head>
<body>
<!--Start preloader-->
<div id="preloader">
<img src="images/loader-big.gif" alt="">
</div>
<!--End preloader-->
<!--Start header-->
<?php
include 'utils-ui/navbar.php';
?>
<!--End header-->
<div class="bannercontainer">
<ul>
 <li data-transition="slotzoom-vertical" data-slotamount="5" data-masterspeed="300">
	<img src="images/slider_img_07.jpg" alt="Backgroud">
	<div class="caption skew_sublayer fade fadeout" data-x="37" data-y="0" data-speed="700" data-start="1500" data-easing="easeOutBack"></div>
	<div class="caption skew_sublayer_02 fade fadeout" data-x="-14" data-y="0" data-speed="700" data-start="1500" data-easing="easeOutBack"></div>
	<div class="caption fade fade-out" data-x="415" data-y="65" data-speed="700" data-start="1500">
		<img class="layer_01" src="images/layerslider_layer_03.png" alt="Background" style="width:340px;height340px;">
	</div>
	<div class="caption fade fade-out" data-x="415" data-y="65" data-speed="500" data-start="1500">
		<img class="layer_02" src="images/layerslider_layer_04.png" alt="Background" style="width:340px;height340px;">
	</div>
	<div class="caption fade fade-out" data-x="418" data-y="65" data-speed="700" data-start="1500">
		<img class="layer_03" src="images/layerslider_layer_05.png" alt="Background" style="width:340px;height340px;">
	</div>
	<div class="caption fade fade-out layer_03" data-x="513" data-y="191" data-speed="700" data-start="1500"><span style="font-size:2em;color:#000;opacity:.6;line-height: 130%;padding:0 0 0 5px;">Ethereal</span><br><span style="font-size:3.5em;color:#00BEB9;line-height: 90%;opacity:.8;">Vista</span></div>
	<div class="caption lft layer_text_type_01" data-x="147" data-y="140" data-speed="500" data-end="400" data-easing="easeOutQuart" data-start="1780">HTML5 CSS3</div>
	<div class="caption lft layer_text_type_01" data-x="952" data-y="188" data-speed="500"  data-easing="easeOutQuart" data-start="1850">PHP</div>
	<div class="caption lfb layer_text_type_01" data-x="267" data-y="264" data-speed="500"  data-easing="easeOutQuart" data-start="2020">JAVASCRIPT</div>
	<div class="caption lft layer_text_type_01" data-x="933" data-y="323" data-speed="500"  data-easing="easeOutQuart" data-start="1890">WIREFRAME</div>
	<div class="caption lft layer_text_type_02" data-x="75" data-y="175" data-speed="500"  data-easing="easeOutQuart" data-start="2060">Web Design<br>& Development</div>
	<div class="caption lfb layer_text_type_02 t_align_left" data-x="753" data-y="259" data-speed="1000"  data-easing="easeOutQuart" data-start="1800">Responsive Design</div>
	<div class="caption lft layer_text_type_04" data-x="63" data-y="306" data-speed="500"  data-easing="easeOutQuart" data-start="1900">User Interface</div>
	<div class="caption lfb layer_text_type_04" data-x="759" data-y="233" data-speed="500"  data-easing="easeOutQuart" data-start="2070">mobile-friendly</div>
	<div class="caption lfb layer_text_type_05" data-x="177" data-y="360" data-speed="500"  data-easing="easeOutQuart" data-start="1940">Static Design </div>
	<div class="caption lft layer_text_type_05" data-x="768" data-y="150" data-speed="500"  data-easing="easeOutQuart" data-start="1910">Web Development</div>
</li>
<li data-transition="random" data-slotamount="5" data-masterspeed="300">
	<img src="images/slider_img_06.jpg" alt="Background">
	<div class="caption skew_sublayer fade fadeout" data-x="37" data-y="0" data-speed="700" data-start="1500" data-easing="easeOutBack"></div>
	<div class="caption skew_sublayer_02 fade fadeout" data-x="-14" data-y="0" data-speed="700" data-start="1500" data-easing="easeOutBack"></div>
	<div class="caption fade fade-out" data-x="415" data-y="65" data-speed="700" data-start="1500">
		<img class="layer_01" src="images/layerslider_layer_03.png" alt="Background" style="width:340px;height340px;">
	</div>
	<div class="caption fade fade-out" data-x="415" data-y="65" data-speed="500" data-start="1500">
		<img class="layer_02" src="images/layerslider_layer_04.png" alt="Background" style="width:340px;height340px;">
	</div>
	<div class="caption fade fade-out" data-x="418" data-y="65" data-speed="700" data-start="1500">
		<img class="layer_03" src="images/layerslider_layer_05.png" alt="Background" style="width:340px;height340px;">
	</div>
	<div class="caption fade fade-out layer_03" data-x="513" data-y="191" data-speed="700" data-start="1500"><span style="font-size:2em;color:#000;opacity:.6;line-height: 130%;padding:0 0 0 5px;">Ethereal</span><br><span style="font-size:3.5em;color:#00BEB9;line-height: 90%;opacity:.8;">Vista</span></div>
	<div class="caption lft layer_text_type_01" data-x="80" data-y="160" data-speed="500" data-end="400" data-easing="easeOutQuart" data-start="1780">WELL-DOCUMENTED CLEAN CODE</div>
	<div class="caption lft layer_text_type_01" data-x="772" data-y="140" data-speed="500"  data-easing="easeOutQuart" data-start="1850">PRESS RELEASE<br>SUBMISSION</div>
	<div class="caption lfb layer_text_type_01" data-x="320" data-y="300" data-speed="500"  data-easing="easeOutQuart" data-start="2020">DISPLAY<br>MARKETING</div>
	<div class="caption lft layer_text_type_01" data-x="883" data-y="358" data-speed="500"  data-easing="easeOutQuart" data-start="1890">ARTICLE<br> SUBMISSION</div>
	<div class="caption lft layer_text_type_02" data-x="300" data-y="225" data-speed="500"  data-easing="easeOutQuart" data-start="2060">SEO<br></div>
	<div class="caption lfb layer_text_type_02 t_align_left" data-x="753" data-y="300" data-speed="1000"  data-easing="easeOutQuart" data-start="1800">CONTENT MARKETING<br></div>
	<div class="caption lft layer_text_type_04" data-x="63" data-y="260" data-speed="500"  data-easing="easeOutQuart" data-start="1900">Internet Marketing</div>
	<div class="caption lfb layer_text_type_04" data-x="779" data-y="243" data-speed="500"  data-easing="easeOutQuart" data-start="2070">Email Marketing</div>
	<div class="caption lfb layer_text_type_05" data-x="110" data-y="376" data-speed="500"  data-easing="easeOutQuart" data-start="1940">Facebook Promotion</div>
	<div class="caption lft layer_text_type_05" data-x="830" data-y="200" data-speed="500"  data-easing="easeOutQuart" data-start="1910">Youtube Promotion</div>
</li>
<li data-transition="random" data-slotamount="5" data-masterspeed="300">
	<img src="images/slider_img_05.jpg" alt="Background">
	<div class="caption skew_sublayer fade fadeout" data-x="37" data-y="0" data-speed="700" data-start="1500" data-easing="easeOutBack"></div>
	<div class="caption skew_sublayer_02 fade fadeout" data-x="-14" data-y="0" data-speed="700" data-start="1500" data-easing="easeOutBack"></div>
	<div class="caption fade fade-out" data-x="415" data-y="65" data-speed="700" data-start="1500">
		<img class="layer_01" src="images/layerslider_layer_03.png" alt="Background" style="width:340px;height340px;">
	</div>
	<div class="caption fade fade-out" data-x="415" data-y="65" data-speed="500" data-start="1500">
		<img class="layer_02" src="images/layerslider_layer_04.png" alt="Background" style="width:340px;height340px;">
	</div>
	<div class="caption fade fade-out" data-x="418" data-y="65" data-speed="700" data-start="1500">
		<img class="layer_03" src="images/layerslider_layer_05.png" alt="Background" style="width:340px;height340px;">
	</div>
	<div class="caption fade fade-out layer_03" data-x="513" data-y="191" data-speed="700" data-start="1500"><span style="font-size:2em;color:#000;opacity:.6;line-height: 130%;padding:0 0 0 5px;">Ethereal</span><br><span style="font-size:3.5em;color:#00BEB9;line-height: 90%;opacity:.8;">Vista</span></div>
	<div class="caption lft layer_text_type_01" data-x="157" data-y="150" data-speed="500" data-end="400" data-easing="easeOutQuart" data-start="1780">ILLUSTRATOR</div>
	<div class="caption lft layer_text_type_01" data-x="772" data-y="178" data-speed="500"  data-easing="easeOutQuart" data-start="1850">STATIONERY PACKAGES</div>
	<div class="caption lfb layer_text_type_01" data-x="260" data-y="310" data-speed="500"  data-easing="easeOutQuart" data-start="2020">CATALOGS</div>
	<div class="caption lft layer_text_type_01" data-x="763" data-y="363" data-speed="500"  data-easing="easeOutQuart" data-start="1890">ADVERTISING</div>
	<div class="caption lft layer_text_type_02" data-x="75" data-y="190" data-speed="500"  data-easing="easeOutQuart" data-start="2060">Graphic Design</div>
	<div class="caption lfb layer_text_type_02 t_align_left" data-x="753" data-y="259" data-speed="1000"  data-easing="easeOutQuart" data-start="1800"><br></div>
	<div class="caption lft layer_text_type_04" data-x="163" data-y="260" data-speed="500"  data-easing="easeOutQuart" data-start="1900">PHOTOSHOP</div>
	<div class="caption lfb layer_text_type_04" data-x="810" data-y="245" data-speed="500"  data-easing="easeOutQuart" data-start="2070">POSTERS</div>
	<div class="caption lfb layer_text_type_05" data-x="147" data-y="360" data-speed="500"  data-easing="easeOutQuart" data-start="1940">BROCHURE</div>
	<div class="caption lft layer_text_type_05" data-x="838" data-y="300" data-speed="500"  data-easing="easeOutQuart" data-start="1910">BILLBOARD</div>
</li>
</ul>
</div>
<!--End slider part-->
<!--Start main section-->
<section class="main" data-animate-up="header-static" data-animate-down="header-small">
<section class="ow_wrap">
<div class="container">
<!--Start our work section-->
<section class="our_work_section">
	<div class="row">
		<div class="span4">
			<figure class="our_work_item slide-to-left">
				<figcaption>
					<h2>Domain Expertise</h2>
				</figcaption>
				<p>Our perceptive workforce provides the best possible solution.</p>

				<i class="check_icon_type">
					<img src="images/home_icon_01.png" alt="Star">
				</i>
				<div class="hex_elem_rounded_type_2 fade ow_icon">
					<span><i class="icon-star"></i></span>
					<span class="h_el_01"></span>
					<span class="h_el_02"></span>									
				</div>
				<div class="counting_item fade">
					<div class="cornice"><span></span><span></span><span></span></div>
					<dl class="counting_inner">
						<dt>4+</dt>
						<dd>years <br> of experience</dd>
					</dl>
					<i class="dashed_stripe"></i>
					<i class="counting_tape"><img src="images/home_icon_06.png" alt="Stripe"></i>
				</div>
				<span class="ow_triangle"></span>
			</figure>
		</div>
		<div class="span4">
			<figure class="our_work_item">
				<figcaption>
					<h2>Affordable Services</h2>
				</figcaption>
				<p>Our prices are modest. We don't believe in ripping-off.</p>
				<i class="check_icon_type">
					<img src="images/home_icon_02.png" alt="Services">
				</i>
				<div class="hex_elem_rounded_type_2 ow_icon">
					<span><i class="icon-cogs"></i></span>
					<span class="h_el_01"></span>
					<span class="h_el_02"></span>
				</div>
				<div class="counting_item fade">
					<div class="cornice"><span></span><span></span><span></span></div>
					<dl class="counting_inner">
						<dt>25</dt>
						<dd>projects &<br>counting</dd>
					</dl>
					<i class="dashed_stripe"></i>
					<i class="counting_tape_type_02">5</i>
				</div>
				<span class="ow_triangle"></span>
			</figure>
		</div>
		<div class="span4">
			<figure class="our_work_item slide-to-right">
				<figcaption>
					<h2>Agile Delivery</h2>
				</figcaption>
				<p>We provide quick services within stipulated time.</p>
				<i class="check_icon_type">
					<img src="images/home_icon_03.png" alt="Settings">
				</i>
				<div class="hex_elem_rounded_type_2 fade ow_icon">
					<span><i class="icon-cog"></i></span>
					<span class="h_el_01"></span>
					<span class="h_el_02"></span>
				</div>
				<span class="ow_triangle"></span>
			</figure>
		</div>
	</div>
</section>
<!--End our work section-->
</div>
</section>
<!--Start  recent projects carousel-->
<div class="container t_align_center grey_text_color">
<h2>Recent Projects</h2>
<p>We have a wide array of projects under our belt, ranging from web designing, web development, graphic design and what not!</p>
</div>

<section class="rp_wrap relative">
<ul class="recent_projects">
<?php
$database = new mainDB();
$result = $database->fetchImage();
$counter = 0;
while($row = $result->fetch_assoc())
{
	$project_name = $database->fetch_project_name($row['project_id']);
	$large_path = $database->fetch_large_image_path($row['project_id']);
	if($counter%2 == 0)
	{
		printf('<li data-type="video-item">');
	}
	if($row['include'] == 1)
	{	
	printf('<figure>
			<img src="images/'.$row['filepath'].'" alt="'.$project_name.'">
			<figcaption>
				<h5>'.$project_name.'</h5>
				<p>Phasellus tincidunt accumsan dictum. Nullam in posuere justo. Ut tincidunt nisl sapien, eget gravida quam vestibulum vitae.</p>
				<hr>
				<a class="go_to_photo" href="images/'.$large_path.'" data-rel="prettyPhoto[recentProjects]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>
				<!--<a class="go_to_single not_animate" href="#" data-rel="prettyPhoto[recentProjects]">
					<span class="active_icon"></span>
					<span class="colorscheme_icon"></span>
				</a>-->
			</figcaption>
		</figure>');
	$counter++;
}
	if($counter%2 == 1)
		continue;
	printf('</li>');
}
?>
</ul>
<button class="rp_button_prev"></button>
<button class="rp_button_next"></button>
</section>

<!--End recent projects carousel-->
<!--Start customer reviews section-->
<section class="customer_reviews">
<div class="container">
	<div class="heading_container_type_02 fade-to-bottom">
		<img class="left_arrow" src="images/home_icon_12.png" alt="Left">
		<h2>What Our Customers Say About Us</h2>
		<img class="right_arrow" src="images/home_icon_13.png" alt="Right">
	</div>
	<div class="row">
	<!--Start customer review-->
		<div class="span6">
			<figure class="clearfix customer_r fade-to-right">
				<!--Start photo-->
				<div class="hexagon_container f_left">
					<canvas class="hexagon" width="200" height="200"></canvas>
					<img src="images/home_img_03.jpg" alt="Vinay Gautam">
				</div>
				<!--End photo-->
				<!--Start comment-->
				<div class="customer_comment">
					<h6><span class="bold">Vinay Gautam</span></h6>
					<p>Ethereal Vista had been a great choice. 
						Their team is devoted to its work and provides
						 instant response to any inquiry. Their in-depth knowledge of the core technologies is impressive. And, their attention to detail is astounding.</p>
					<!--Start social icons-->
					<ul class="social_icons">
						<li><a href="#"><i class="icon-tumblr"></i></a></li>
						<li><a href="#"><i class="icon-twitter"></i></a></li>
						<li><a href="#"><i class="icon-skype"></i></a></li>
					</ul>
					<!--End social icons-->
				</div>
				<!--End comment-->
			</figure>
		</div>
	<!--End customer review-->
	<!--Start customer review-->
		<div class="span6">
			<figure class="clearfix t_align_right customer_r fade-to-left">
				<!--Start photo-->
				<div class="hexagon_container f_right">
					<canvas class="hexagon" width="200" height="200"></canvas>
					<img src="images/home_img_04.jpg" alt="Akansha Mittal">
				</div>
				<!--End photo-->
				<!--Start comment-->
				<div class="customer_comment">
					<h6><span class="bold">Akansha Mittal</span></h6>
					<p>Honestly, took a chance with this one but it turned out to be better than I had expected. Ethereal Vista provides fast service and has a talented team.
						They understand your problem and provide suitable soution. Quite satisfied by their work and performance.
					</p>
					<!--Start social icons-->
					<ul class="social_icons clearfix">
						<li><a href="#"><i class="icon-tumblr"></i></a></li>
						<li><a href="#"><i class="icon-twitter"></i></a></li>
						<li><a href="#"><i class="icon-skype"></i></a></li>
					</ul>
					<!--End social icons-->
				</div>
				<!--End comment-->
			</figure>
		</div>
	<!--End customer review-->
	</div>
</div>
</section>
<!--End customer reviews section-->
<!--Start parallax section-->
<section class="parallax parallax_s">
<div class="container">
	<h1 class="skew-to-left">We create beautifully-crafted websites that stand out from the crowd.</h1>
	<div class="inline skew-to-left">
		<a class="button_style_01 m_top_20" href="portfolio.php">See how we prove it</a>
	</div>
</div>
</section>
<!--End parallax section-->
<!--Start color scheme section-->
<section class="color_scheme_section">
<div class="heading_container_type_03 fade-to-bottom">
	<h1>Our Process</h1>
</div>
<!--Start steps-->
<div class="container clearfix">
	<figure class="show_step_container clearfix">
		<figcaption>
			<h5 class="fade">Plan</h5>
		</figcaption>
		<div class="icon_container center-to-ls3">
			<div class="icon_parent"></div>
			<div class="cornice_button type_2"><span></span><span></span><span></span></div>
			<div class="icon_inner">
				<img src="images/home_icon_15.png" alt="search">
			</div>
			<div class="step_icon fade">
				step 1
				<i></i>
				<i></i>
				<div><img src="images/home_icon_20.png" alt="Step 1"></div>
			</div>
		</div>
		<p class="triangle_shape_float fade">
			<!--Start span's for the text to wrap the shape-->
			<span></span><span></span><span></span><span></span><span></span><span></span>
			<!--End div's for the text to wrap the shape-->
				We analyze the client's requirements, &nbsp;&nbsp; brainstorm and devise <br>a plan accordingly.</p>
	</figure>
	<figure class="show_step_container">
		<figcaption>
			<h5 class="fade">Design &amp; Develop</h5>
		</figcaption>
		<div class="icon_container clearfix center-to-ls1">
			<div class="icon_parent"></div>
			<div class="cornice_button type_2"><span></span><span></span><span></span></div>
			<div class="icon_inner">
				<img src="images/home_icon_17.png" alt="Check Screen">
			</div>
			<div class="step_icon fade">
				step 2
				<i></i>
				<i></i>
				<div><img src="images/home_icon_20.png" alt="Step 2"></div>
			</div>
		</div>
		<p class="triangle_shape_float fade">
			<!--Start span's for the text to wrap the shape-->
			<span></span><span></span><span></span><span></span><span></span><span></span>
			<!--End div's for the text to wrap the shape-->
				Our team of <br>highly qualified professionals provide <br>the best-in-class <br>service to our Clients.</p>
	</figure>
	<figure class="show_step_container">
		<figcaption>
			<h5 class="fade">Deploy</h5>
		</figcaption>
		<div class="icon_container clearfix center-to-ls2">
			<div class="icon_parent"></div>
			<div class="cornice_button type_2"><span></span><span></span><span></span></div>
			<div class="icon_inner">
				<img src="images/home_icon_18.png" alt="Person">
			</div>
			<div class="step_icon fade">
				step 3
				<i></i>
				<i></i>
				<div><img src="images/home_icon_20.png" alt="Step 3"></div>
			</div>
		</div>
		<p class="triangle_shape_float fade">
			<!--Start span's for the text to wrap the shape-->
			<span></span><span></span><span></span><span></span><span></span><span></span>
			<!--End div's for the text to wrap the shape-->
				We can certainly<br> measure upto <br>our Client's expectations.<br> Our work speaks for itself.</p>
		<div class="icon_container last center-to-ls4"> 
			<div class="icon_parent"></div>
			<div class="cornice_button type_2"><span></span><span></span><span></span></div>
			<div class="icon_inner">
				<img src="images/home_icon_19.png" alt="Shopping Bag">
			</div>
		</div>
	</figure>
</div>
</section>
<!--End color scheme section-->
<!--Start blockquotes section-->
<section class="blockquotes_section">
<!--Start carousel-->
<div class="container">
	<div class="relative">
		<ul class="blockquotes_carousel default">
			<li>
				<blockquote class="type_2 fade-to-right">
					<!--Start div's for the text to wrap the shape-->
					<div></div><div></div><div></div><div></div><div></div><div></div>
					<div></div><div></div><div></div><div></div><div></div>
					<!--End div's for the text to wrap the shape-->
					<p>If there’s one thing you learn<br>by working on a lot of different<br> Web sites, it’s that almost<br> any design idea– </p>
				</blockquote>
				<div class="testimonial_author_container fade-to-top">
					<div class="photo_container">
						<div class="hexagon_small_container">
							<img src="images/steve_krug.jpg" alt="Steve Krug">
							<canvas width="114" height="128"></canvas>
						</div>
						<span class="color_scheme">Steve Krug, </span><span class="black">Don't make me think.</span>
					</div>
					<span></span><span></span>
				</div>
				<blockquote class="type_2 fade-to-left">
					<!--Start div's for the text to wrap the shape-->
					<div></div><div></div><div></div><div></div><div></div><div></div>
					<div></div><div></div><div></div><div></div><div></div>
					<!--End div's for the text to wrap the shape-->
					<p>no matter how appallingly bad<br>–can be made usable in the<br> right circumstances,<br> with enough effort.</p>
				</blockquote>
			</li>
			<li>
				<blockquote class="type_2">
					<!--Start div's for the text to wrap the shape-->
					<div></div><div></div><div></div><div></div><div></div><div></div>
					<div></div><div></div><div></div><div></div><div></div>
					<!--End div's for the text to wrap the shape-->
					<p>A designer knows he has achieved<br> perfection not when there<br> is nothing left to add,</p>
				</blockquote>
				<div class="testimonial_author_container">
					<div class="photo_container">
						<div class="hexagon_small_container">
							<img src="images/antoine-de-saint-exupery.jpg" alt="">
							<canvas width="114" height="128"></canvas>
						</div>
						<span class="color_scheme">Antoine de Saint-Exupéry</span><span class="black"></span>
					</div>
					<span></span><span></span>
				</div>
				<blockquote class="type_2">
					<!--Start div's for the text to wrap the shape-->
					<div></div><div></div><div></div><div></div><div></div><div></div>
					<div></div><div></div><div></div><div></div><div></div>
					<!--End div's for the text to wrap the shape-->
					<p>but when there is nothing<br> left to take away.</p>
				</blockquote>
			</li>
		</ul>
		<button class="blockquotes_carousel_prev fade"><span class="icon-chevron-left"></span></button>
		<button class="blockquotes_carousel_next fade"><span class="icon-chevron-right"></span></button>
	</div>
	<!--End carousel-->
</div>
</section>
<!--End blockquotes section-->
<!--Start register-->	
<section class="register_container type1 parallax">	
<div class="container">
	<div class="inline wind">
		<h2 class="slog">Grab this work!</h2>
		<p style="font-size:20px;">To know of all the latest design trends, sign up for our weekly newsletter!</p>
	</div>
	<form action="<?php $_SERVER['PHP_SELF'];?>" method="post" class="form_style_01 wind">
		<input type="email" name="subscriber_email" style="border-color:black;"placeholder="Email address" required>
		<input type="submit" name="subscribe" value="Sign up now!">
	</form>
</div>
</section>
<!--End register-->
</section>
<!--End main section-->
<!--Start footer-->
<?php
include 'utils-ui/footer.php';
?>
<!--End footer-->
<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="js\jquery.themepunch.plugins.min.js"></script>
		<script type="text/javascript" src="js\jquery.themepunch.revolution.js"></script>
		<script type="text/javascript" src="js\jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="js\retina.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js\owl.carousel.min.js"></script>
		<script type="text/javascript" src="js\jquery.easing-1.3.min.js"></script>
		<script type="text/javascript" src="js\jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="js\parallax.js"></script>
		<script type="text/javascript" src="js\smoothscroll.js"></script>
		<script type="text/javascript" src="js\jquery.carouFredSel-6.0.3-packed.js"></script>
		
		<script type="text/javascript" src="js\styleswitcher.js"></script>
		<script type="text/javascript" src="js\scripts.js"></script>
		<!--End scripts include-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80174278-2', 'auto');
  ga('send', 'pageview');

</script>		
</body>
</html>

<?php
if(isset($_POST['subscribe']))
{
$db = new mainDB();
$email = $_POST['subscriber_email'];
$val = $db->add_subscriber($email);
if($val)
echo '<head><script>alert("Thank you for subscribing!");</script></head>';
else
echo '<head><script>alert("Sorry for the inconvenience. We\'ll be up soon!");</script></head>';
}
?>