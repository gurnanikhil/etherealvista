<!DOCTYPE HTML>
<html lang="en" class="no-js">
<head>
<title>Our services | ETHEREAL VISTA</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="Team Ethereal Vista">
		<meta name="description" content="Ethereal Vista based in Delhi, provides web design and development, graphic design, and digital marketing services.">
		<meta name="keywords" content="web services,web solutions,website design company,digital marketing company,seo company">
<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
<!--End meta info-->
<!--Start stylesheet include-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">

<!--End stylesheet include-->
<!--Start scripts include-->
<script type="text/javascript" src="js/modernizr.js"></script>
<!--End scripts include-->

<link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">

</head>
<body>
<!--Start preloader-->
<div id="preloader">
<img src="images/loader-big.gif" alt="Please Wait">
</div>
<!--End preloader-->
<!--Start header-->
<?php
include 'utils-ui/navbar.php';
?>
<!--End header-->
<!--Start main section-->
<section class="main secondary_page" data-animate-up="header-static" data-animate-down="header-small">
<!--Start title container-->
<div class="title_container type_3 t_align_center">
<div class="container">
<ul class="path_list clearfix">
<li><a href="index.php">Home</a></li>
<li><i></i><a href="our_services.php">Our Services</a></li>
</ul>
<h1>Services</h1>
</div>
</div>
<section class="grey_text_color">
<div class="container">
<!--Start service style 1-->
<section class="service_container">
<div class="row">
<div class="span3">
	<figure class="service">
		<div class="image_hex">
			<div class="hex_elem_rounded_type_2">
				<span class="h_el_01"></span>
				<span class="h_el_02"></span>
				<div class="hex_elem_rounded_type_2 services_type_2">
					<img src="images/services_img_01.png" alt="Quality Product">
					<span class="light_shadow"></span>
					<span class="count">15</span>
					<span class="h_el_01"></span>
					<span class="h_el_02"></span>
				</div>
			</div>
			<span class="stripe"></span>
			<span class="circle"></span>
		</div>
		<span class="ok_icon first">
			<span class="icon-ok"></span>
		</span>
		<figcaption>
			<h4>Quality Product</h4>
			<p>Quality is the one thing we never compromise with, no matter what. We only work for our cusmtomer's satisfaction.</p>
		</figcaption>
	</figure>
</div>
<div class="span3">
	<figure class="service">
		<div class="image_hex">
			<div class="hex_elem_rounded_type_2">
				<span class="h_el_01"></span>
				<span class="h_el_02"></span>
				<div class="hex_elem_rounded_type_2 services_type_2">
					<img src="images/services_img_02.png" alt="Dedicated Team">
					<span class="light_shadow"></span>
					<span class="count">21</span>
					<span class="h_el_01"></span>
					<span class="h_el_02"></span>
				</div>
			</div>
			<span class="stripe"></span>
			<span class="circle"></span>
		</div>
		<span class="ok_icon second">
			<span class="icon-ok"></span>
		</span>
		<figcaption>
			<h4>Dedicated Team</h4>
			<p>We have a team who is dedicated to its work, working day and night to deliver the best.</p>
		</figcaption>
	</figure>
</div>
<div class="span3">
	<figure class="service">
		<div class="image_hex">
			<div class="hex_elem_rounded_type_2">
				<span class="h_el_01"></span>
				<span class="h_el_02"></span>
				<div class="hex_elem_rounded_type_2 services_type_2">
					<img src="images/services_img_03.png" alt="Full Suport">
					<span class="light_shadow"></span>
					<span class="count">19</span>
					<span class="h_el_01"></span>
					<span class="h_el_02"></span>
				</div>
			</div>
			<span class="stripe"></span>
			<span class="circle"></span>
		</div>
		<span class="ok_icon last">
			<span class="icon-ok"></span>
		</span>
		<figcaption>
			<h4>Full Support</h4>
<p>We believe in providing full assistance to our customers. No matter what, we are always there to resolve your queries.</p></figcaption>
	</figure>
</div>
<div class="span3">
	<figure class="service">
		<div class="image_hex">
			<div class="hex_elem_rounded_type_2">
				<span class="h_el_01"></span>
				<span class="h_el_02"></span>
				<div class="hex_elem_rounded_type_2 services_type_2">
					<img src="images/services_img_05.png" alt="Knowledge">
					<span class="light_shadow"></span>
					<span class="count">19</span>
					<span class="h_el_01"></span>
					<span class="h_el_02"></span>
				</div>
			</div>
			<span class="stripe"></span>
			<span class="circle"></span>
		</div>
		<figcaption>
			<h4>Knowledge</h4>
			<p>Our biggest strength is our never ending knowledge of not just the technical aspect but also the matters concerning business.</p>
		</figcaption>
	</figure>
</div>
</div>
</section>
<!--End service style 1-->
</div>
<!--Start our work area-->
<section class="our_work_section_2">
<div class="container relative clearfix">
<div class="our_work_services_item">
<h5><a href="website_development.php">Web Development</a></h5>
<p>Developing websites as per the customer's requirements using the latest technologies.</p>
<div class="our_work_icon">
	<img src="images/our_work_icon_01.png" alt="Web Development">
</div>
</div>
<div class="our_work_services_item" >
<div class="our_work_icon">
	<img src="images/our_work_icon_02.png" alt="Marketing">
</div>
<h5><a href="#">Marketing</a></h5>
<p>Promoting customer's website on popular social media platforms and improving its ranking on popular search engines.</p>
</div>
<div class="our_work_services_item">
<div class="our_work_icon">
	<img src="images/our_work_icon_03.png" alt="Web &amp; Design">
</div>
<h5><a href="#">Web &amp; Design</a></h5>
<p>Designing a website or redesigning an existing website.</p>
</div>
<div class="our_work_services_item">
<h5><a href="#">Graphic Design</a></h5>
<p>Providing a wide variety of services in the field of Graphics.</p>
<div class="our_work_icon">
	<img src="images/our_work_icon_04.png" alt="Graphic Design">
</div>
</div>
<div class="our_work_title">
<figure>
	<figcaption>
		<h1>Yes!</h1>
	</figcaption>
	<p>We have what it takes to get the job done.</p>
</figure>
<div class="title_cornice"></div>
</div>
</div>
</section>
<!--End our work area-->
<!--Start carousel-->
<div class="container">
<div class="relative">
<ul class="blockquotes_carousel services">
<li>
	<blockquote class="type_2">
		<!--Start div's for the text to wrap the shape-->
		<div></div><div></div><div></div><div></div><div></div><div></div>
		<div></div><div></div><div></div><div></div><div></div>
		<!--End div's for the text to wrap the shape-->
		<p>Ethereal Vista worked on a project for my company and we were happy by the way they handled the project.</p>
	</blockquote>
	<div class="testimonial_author_container">
		<div class="photo_container">
			<div class="hexagon_small_container">
				<img src="images/vinay_saraogi.jpg" alt="Vinay Saraogi"> 
				<canvas width="114" height="128"></canvas>
			</div> 
			<span class="color_scheme">Vinay </span><span class="black">Saraogi</span>  
		</div>
		<span></span><span></span>
	</div>
	<blockquote class="type_2">
		<!--Start div's for the text to wrap the shape-->
		<div></div><div></div><div></div><div></div><div></div><div></div>
		<div></div><div></div><div></div><div></div><div></div>
		<!--End div's for the text to wrap the shape-->
		<p>They provided help whenever we needed it. They easily accommodated all the changes that we wanted them to in the project.</p>
	</blockquote>
</li>
<li>
	<blockquote class="type_2">
		<!--Start div's for the text to wrap the shape--> 
		<div></div><div></div><div></div><div></div><div></div><div></div>
		<div></div><div></div><div></div><div></div><div></div>
		<!--End div's for the text to wrap the shape-->
		<p>Liked working with Ethereal Vista. They are worth giving a shot, you won't be disappointed, atleast I wasn't.</p>
	</blockquote>
	<div class="testimonial_author_container">
		<div class="photo_container">
			<div class="hexagon_small_container">
				<img src="images/navdeep_sharma.jpg" alt="Navdeep Sharma">  
				<canvas width="114" height="128"></canvas>
			</div>
			<span class="color_scheme">Navdeep </span><span class="black">Sharma</span>
		</div>
		<span></span><span></span>
	</div>
	<blockquote class="type_2">
		<!--Start div's for the text to wrap the shape-->
		<div></div><div></div><div></div><div></div><div></div><div></div>
		<div></div><div></div><div></div><div></div><div></div>
		<!--End div's for the text to wrap the shape-->
		<p>Their best quality is from the fact that they value their customer's opinions. I haven't witnessed this in any other company.</p>
	</blockquote>
</li>
</ul>
<button class="blockquotes_carousel_prev"><span class="icon-chevron-left"></span></button>
<button class="blockquotes_carousel_next"><span class="icon-chevron-right"></span></button>
</div>
</div>
<!--End carousel-->
<section class="register_container parallax type1">
<div class="container t_align_center">
<h1 class="default">Use our services for your project.</h1>
<!--Start social icons-->
<ul class="social_icons_type_03 inline clearfix">
	<li class="twitter">
		<a href="https://twitter.com/ethereal_vista" class="hex_elem_rounded_type_2">
			<span class="s_icon"></span>
			<span class="h_el_01"></span>
			<span class="h_el_02"></span>
		</a>
	</li>

	<li class="facebook">
		<a href="https://www.facebook.com/Ethereal-Vista-223083851416366" class="hex_elem_rounded_type_2">
			<span class="s_icon"></span>
			<span class="h_el_01"></span>
			<span class="h_el_02"></span>
		</a>
	</li>								 
</ul>
<!--End social icons-->
</div>
</section>
</section>
</section>
<!--End main section-->
<!--Start footer-->
<?php
	include 'utils-ui/footer.php';
?>
<!--End footer-->
<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/jquery.carouFredSel-6.0.3-packed.js"></script>
		<script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80174278-2', 'auto');
  ga('send', 'pageview');

</script>	
</body>
</html>