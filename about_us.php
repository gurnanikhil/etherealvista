<!DOCTYPE HTML>
<html lang="en" class="no-js">
<head>
		<title>About Us | Ethereal Vista</title>
		<!--Start meta info-->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta charset="utf-8">
		<meta name="author" content="Team Ethereal Vista">
		<meta name="description" content="Ethereal Vista is a leading website development and design, graphic design and digital marketing company, based in Delhi NCR.">
		<meta name="keywords" content="website development company,graphic design company,digital marketing company,seo company,web solution services">
<!--[if IE]><meta http-equiv="X-UA-Compitible" content="IE=edge,chrome=1"><![endif]-->
<!--End meta info-->
<!--Start stylesheet include-->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css" id="style">
<link rel="stylesheet" type="text/css" href="css/flexslider.css">
<link rel="stylesheet" type="text/css" href="css/styleswitcher.css">

<!--End stylesheet include-->
<!--Start scripts include-->
<script type="text/javascript" src="js/modernizr.js"></script>
<!--End scripts include-->

<link rel="icon" type="image/png" href="favicon.ico" sizes="16x16">

</head>
<body>
<!--Start preloader-->
<div id="preloader">
<img src="images/loader-big.gif" alt="Please Wait">
</div>
<!--End preloader-->
<!--Start header-->
<?php
include 'utils-ui/navbar.php';
?>
<!--End header-->
<!--Start main section-->
<section class="main secondary_page" data-animate-up="header-static" data-animate-down="header-small">
<!--Start title container-->
<div class="title_container t_align_center type_3">
<div class="container">
<ul class="path_list clearfix">
<li><a href="index.php">Home</a></li>
<li><i></i><a href="about_us.php">About Us</a></li>
</ul>
<h1>About Us</h1>
</div>
</div>
<!--End title container-->
<!--Start about us section-->
<main class="about_us">
<div class="container">
<div class="row">
<figure class="span4">
<figcaption>
	<h2>Who We Are?</h2>
</figcaption>
<p>Ethereal Vista is a global web solutions company, launched with a belief of providing quality work with effective results and above all, meeting customer's budget requirements. Our customer's satisfaction is our prime goal. </p>
<ul class="icons_type_01 clearfix">
	<li>
		<i class="icons_type_01_like"><span></span></i>
	</li>
	<li>
		<i class="icons_type_01_circle"><span></span></i>
	</li>
	<li>
		<i class="icons_type_01_winner"><span></span></i>
	</li>
	<li>
		<i class="icons_type_01_mail"><span></span></i>
	</li>
	<li>
		<i class="icons_type_01_world"><span></span></i>
	</li>
</ul>
</figure>
<figure class="span4 offset4 t_align_right">
<figcaption>
	<h2>What We Do?</h2>
</figcaption>
<p>Ethereal Vista provides services in the areas of graphics, web and marketing. We believe in providing the best quality services to our customers by incorporating our technical expertise, knowledge of latest industry trends and creativity into every project we get our hands on. </p>
<ul class="icons_type_01 media_f_right clearfix">
	<li>
		<i class="icons_type_01_percent"><span></span></i>
	</li>
	<li>
		<i class="icons_type_01_phone"><span></span></i>
	</li>
	<li>
		<i class="icons_type_01_percent"><span></span></i>
	</li>
	<li>
		<i class="icons_type_01_eye"><span></span></i>
	</li>
	<li>
		<i class="icons_type_01_rating"><span></span></i>
	</li>
</ul>
</figure>
</div>
</div>
</main>
<!--End about us section-->
<!--Start skills section-->
<section class="skills">
<div class="container o_hide">
<div class="t_align_center skills_title_container about">
<div class="skills_title_inner">
<img src="images/skills_icon.png" alt="Skills">
<h2>Our Skills</h2>
</div>
<i></i>
</div>
</div>
<div class="skills_content_part">
<div class="line"></div>
<div class="container">
<div class="skills_wrap clearfix">
<div class="skill_item active">
	<div class="clean_cornice"></div>
	<div class="inner">95%</div>
</div>
<div class="skill_title">
	HTML &amp; CSS
	<div></div>
</div>
<div class="skill_item">
	<div class="clean_cornice"></div>
	<div class="inner">80%</div>
</div>
<div class="skill_title">
	Jquery
	<div></div>
</div>
<div class="skill_item active">
	<div class="clean_cornice"></div>
	<div class="inner">90%</div>
</div>
<div class="skill_title">
	Bootstrap
	<div></div>
</div>
<div class="skill_item">
	<div class="clean_cornice"></div>
	<div class="inner">85%</div>
</div>
<div class="skill_title">
	Laravel
	<div></div>
</div>
<div class="skill_item active">
	<div class="clean_cornice"></div>
	<div class="inner">90%</div>
</div>
<div class="skill_title">
	Php
	<div></div>
</div>
<div class="skill_item active">
	<div class="clean_cornice"></div>
	<div class="inner">95%</div>
</div>
<div class="skill_title">
	Photoshop
	<div></div>
</div>
<div class="skill_item">
	<div class="clean_cornice"></div>
	<div class="inner">89%</div>
</div>
<div class="skill_title">
	Illustrator
	<div></div>
</div>
<div class="skill_item">
	<div class="clean_cornice"></div>
	<div class="inner">80%</div>
</div>
<div class="skill_title">
	Wordpress
	<div></div>
</div>
<div class="graphic_skills_container" style="position:absolute;">
	<canvas height="300"></canvas>
</div>
</div>
</div>
</div>
</section>
<!--End skills section-->
<!--Start our team section-->
<section class="our_team_section t_align_center">
<div class="container o_hide">
<h2>Meet Our Team</h2>
<div class="row">
<div class="span3" style="padding-left:150px;">
<figure class="team_person">
	<div class="photo">
		<div class="hexagon_big_container">
			<img src="images/about_img_01.jpg" alt="Nikhil Gurnani">
			<canvas width="240" height="270"></canvas>
		</div>
		<div class="link_container hex_elem_rounded_type_2">
			<a href="contact.php" class="hex_elem_rounded_type_2">
				<span>Get In Touch</span>
				<span class="h_el_01"></span>
				<span class="h_el_02"></span>
			</a>
			<span class="h_el_01"></span>
			<span class="h_el_02"></span>
		</div>
	</div>
	<div class="person_title">
		<span class="bold">Nikhil Gurnani</span>
		<span class="color_scheme"><strong>CTO</strong> &amp; Co - Founder</span>
		<ul class="social_icons_type_02 grey_color clearfix">
				<li>
					<a href="#" class="hex_elem_rounded_type_2">
						<span class="icon-twitter"></span>
						<span class="h_el_01"></span>
						<span class="h_el_02"></span>
					</a>
				</li>
				<li>
					<a target="_blank" href="https://www.facebook.com/gurnanikhil" class="hex_elem_rounded_type_2">
						<span class="icon-facebook"></span>
						<span class="h_el_01"></span>
						<span class="h_el_02"></span>
					</a>
				</li>
				<li>
					<a href="#" class="hex_elem_rounded_type_2">
						<span class="icon-linkedin"></span>
						<span class="h_el_01"></span>
						<span class="h_el_02"></span>
					</a>
				</li>
			</ul>
	</div>
</figure>
</div>
<!--profile 1 ends-->

<!--Profile 3 starts-->
<div class="span3">
<figure class="team_person">
	<div class="photo">
		<div class="hexagon_big_container">
			<img src="images/about_img_03.jpg" alt="Raghav Tayal">
			<canvas width="240" height="270"></canvas>
		</div>
		<div class="link_container hex_elem_rounded_type_2">
			<a href="contact.php" class="hex_elem_rounded_type_2">
				<span>Get In Touch</span>
				<span class="h_el_01"></span>
				<span class="h_el_02"></span>
			</a>
			<span class="h_el_01"></span>
			<span class="h_el_02"></span>
		</div>
	</div>
	<div class="person_title">
		<span class="bold">Raghav Tayal</span>
		<span class="color_scheme"><strong>CEO</strong> &amp; Founder</span>
		<ul class="social_icons_type_02 grey_color clearfix">
				<li>
					<a href="#" class="hex_elem_rounded_type_2">
						<span class="icon-twitter"></span>
						<span class="h_el_01"></span>
						<span class="h_el_02"></span>
					</a>
				</li>
				<li>
					<a target="_blank" href="https://www.facebook.com/raghav.tayal.5" class="hex_elem_rounded_type_2">
						<span class="icon-facebook"></span>
						<span class="h_el_01"></span>
						<span class="h_el_02"></span>
					</a>
				</li>
				<li>
					<a href="#" class="hex_elem_rounded_type_2">
						<span class="icon-linkedin"></span>
						<span class="h_el_01"></span>
						<span class="h_el_02"></span>
					</a>
				</li>
			</ul>
	</div>
</figure>
</div>
<!--Profile 3 ends-->

<!--Profile 4 starts-->
<div class="span3">
<figure class="team_person">
	<div class="photo">
		<div class="hexagon_big_container">
			<img src="images/about_img_02.jpg" alt="Ragini Vaid">
			<canvas width="240" height="270"></canvas>
		</div>
		<div class="link_container hex_elem_rounded_type_2">
			<a href="contact.php" class="hex_elem_rounded_type_2">
				<span>Get In Touch</span>
				<span class="h_el_01"></span>
				<span class="h_el_02"></span>
			</a>
			<span class="h_el_01"></span>
			<span class="h_el_02"></span>
		</div>
	</div>
	<div class="person_title">
		<span class="bold">Ragini Vaid</span>
		<span class="color_scheme"><strong>CMO</strong> &amp; Co - Founder</span>
		<ul class="social_icons_type_02 grey_color clearfix">
				<li>
					<a href="#" class="hex_elem_rounded_type_2">
						<span class="icon-twitter"></span>
						<span class="h_el_01"></span>
						<span class="h_el_02"></span>
					</a>
				</li>
				<li>
					<a target="_blank" href="https://www.facebook.com/ragini.vaid" class="hex_elem_rounded_type_2">
						<span class="icon-facebook"></span>
						<span class="h_el_01"></span>
						<span class="h_el_02"></span>
					</a>
				</li>
				<li>
					<a href="#" class="hex_elem_rounded_type_2">
						<span class="icon-linkedin"></span>
						<span class="h_el_01"></span>
						<span class="h_el_02"></span>
					</a>
				</li>
			</ul>
	</div>
</figure>
</div>
</div>
</div>
<!--Profile 4 ends-->
</section>
<!--End our team section-->
<!--Start parallax section-->
<section class="parallax_section_about parallax t_align_center">
<div class="container">
<h2>We create ideas worth marketing.</h2>
<p>Want to know more? Let's talk!</p>
<a href="contact.php" class="button_style_01 m_top_15">Make an enquiry</a>
</div>
</section>
<!--End parallax section-->
<!--Start our clients carousel-->
<section class="our_clients_section">
<div class="container">
<div class="row">
<div class="custom_span_4_5">
<ul class="our_clients_first">
	<li>
		<a href="#">
			<img src="images/our_client_logo_05.png" alt="Client 1">
		</a>
	</li>
	<li>
		<a href="#">
			<img src="images/our_client_logo_04.png" alt="Client 2">
		</a>
	</li>
	<li>
		<a href="#">
			<img src="images/our_client_logo_01.png" alt="Client 3">
		</a>
	</li>
	<li>
		<a href="#">
			<img src="images/our_client_logo_02.png" alt="Client 4">
		</a>
	</li>
</ul>
</div>
<div class="span3">
<figure class="our_clients_title">
	<span></span><span></span>
	<h2>Our Clients</h2>
	<p>We have our client's spread over the globe, and belonging to different industries.</p>
	<a class="our_clients_prev"><span class="icon-chevron-left"></span></a>
	<a class="our_clients_next"><span class="icon-chevron-right"></span></a>
</figure>
</div>
<div class="custom_span_4_5">
<ul class="our_clients">
	<li>
		<a href="#">
			<img src="images/our_client_logo_01.png" alt="Client 5">
		</a>
	</li>
	<li>
		<a href="#">
			<img src="images/our_client_logo_02.png" alt="Client 6">
		</a>
	</li>
	<li>
		<a href="#">
			<img src="images/our_client_logo_03.png" alt="Client 7">
		</a>
	</li>
	<li>
		<a href="#">
			<img src="images/our_client_logo_04.png" alt="Client 8">
		</a>
	</li>
</ul>
</div>
</div>
</div>
</section>
<!--End our clients carousel-->
</section>
<!--End main section-->
<!--Start footer-->
<?php
include 'utils-ui/footer.php';
?>
<!--End footer-->
<!--Start scripts include-->
		<script>window.jQuery || document.write('<script src="js/jquery-1.8.1.min.js">\x3C/script>')</script>
		<script src="js/waypoints.min.js"></script>
		<script type="text/javascript" src="js/retina.js"></script>
		<script type="text/javascript" src="js/smoothscroll.js"></script>
		<script type="text/javascript" src="js/jquery.easing-1.3.min.js"></script>
		<script type="text/javascript" src="js/parallax.js"></script>
		<script type="text/javascript" src="js/jquery.carouFredSel-6.0.3-packed.js"></script>
		<script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
		
		<script type="text/javascript" src="js/styleswitcher.js"></script>
		<script type="text/javascript" src="js/scripts.js"></script>
		<!--End scripts include-->
		
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-80174278-2', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>